$(function(){
    $(".ms-bottombar-ms").on("click", function (){
        $(".ms-bottombar-tools").removeClass("ms-hidden");
        $(".ms-home-bottombar").addClass("ms-maximize");
    });

    $(".ms-bottombar-minimize").on("click", function (){
        $(".ms-bottombar-tools").addClass("ms-hidden");
        $(".ms-home-bottombar").removeClass("ms-maximize");
    });
});