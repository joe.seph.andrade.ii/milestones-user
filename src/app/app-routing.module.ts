import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'loading-screen', pathMatch: 'full' },
  { path: 'login', loadChildren: () => import('./content/login/login.module').then( m => m.LoginPageModule)},
  { path: 'tab1',         loadChildren: './content/tabs/tab1/tab1.module#Tab1PageModule' },
  { path: 'tab3',         loadChildren: './content/tabs/tab3/tab3.module#Tab3PageModule' },
  { path: 'tab4',         loadChildren: './content/tabs/tab4/tab4.module#Tab4PageModule' },
  { path: 'tabs',         loadChildren: './content/tabs/tabs.module#TabsPageModule' },
  { path: 'profile',      loadChildren: './content/profile/profile.module#ProfilePageModule' },
  { path: 'verifypage1',  loadChildren: './content/login/verifyinfo/verifypage1/verifypage1.module#Verifypage1PageModule' },
  { path: 'verifypage2',  loadChildren: './content/login/verifyinfo/verifypage2/verifypage2.module#Verifypage2PageModule' },
  { path: 'verifypage3',  loadChildren: './content/login/verifyinfo/verifypage3/verifypage3.module#Verifypage3PageModule' },
  { path: 'msForm',        loadChildren: './content/createms/msForm/msForm.module#Step1PageModule' },
  { path: 'msChecklist',  loadChildren: './content/createms/msChecklist/msChecklist.module#msChecklistPageModule' },
  { path: 'msItemDetails/:idx', loadChildren: './content/createms/msItemDetails/msItemDetails.module#MsItemDetailsPageModule' },
  { path: 'ms-packages/:idx', loadChildren: './content/createms/ms-packages/ms-packages.module#MsPackagesPageModule' },
  { path: 'chat/:chatId/:idx/:source', loadChildren: './content/createms/chat/chat.module#ChatPageModule' },
  { path: 'view-events', loadChildren: './content/tabs/view-events/view-events.module#ViewEventsPageModule' },
  { path: 'choosetype', loadChildren: './content/createms/choosetype/choosetype.module#ChoosetypePageModule' },
  { path: 'choosecoordinator', loadChildren: './content/createms/choosecoordinator/choosecoordinator.module#ChoosecoordinatorPageModule' },
  // { path: 'modal-details', loadChildren: './content/createms/ms-packages/modal-details/modal-details.module#ModalDetailsPageModule' },
  // { path: 'pref-modal', loadChildren: './content/createms/msChecklist/pref-modal/pref-modal.module#PrefModalPageModule' },
  { path: 'alldeals', loadChildren: './content/tabs/alldeals/alldeals.module#AlldealsPageModule' },
  { path: 'allmerchants', loadChildren: './content/tabs/allmerchants/allmerchants.module#AllmerchantsPageModule' },
  { path: 'promo-details', loadChildren: './content/tabs/promo-details/promo-details.module#PromoDetailsPageModule' },
  { path: 'tab5', loadChildren: './content/tabs/tab5/tab5.module#Tab5PageModule' },
  { path: 'loading-screen', loadChildren: './content/loading-screen/loading-screen.module#LoadingScreenPageModule' },  { path: 'verifyphone', loadChildren: './content/login/verifyinfo/verifyphone/verifyphone.module#VerifyphonePageModule' },
  { path: 'cities', loadChildren: './content/login/verifyinfo/verifypage1/cities/cities.module#CitiesPageModule' },







];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
