import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';

import { AngularFirestoreModule } from '@angular/fire/firestore';


import { Facebook } from '@ionic-native/facebook/ngx';
import { environment } from '../environments/environment';
import { IonicStorageModule } from '@ionic/storage';

import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { EnvServiceProvider } from "./shared/services/env-service.provider";
import { HttpInterceptorService } from "./shared/services/http-interceptor.service";
import { Camera } from '@ionic-native/camera/ngx';
import { CameraPreview } from '@ionic-native/camera-preview/ngx';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {WebView} from '@ionic-native/ionic-webview/ngx';

@NgModule({
  declarations: [
    AppComponent,
  ],
  entryComponents: [
  ],
  imports: 
    [
      BrowserModule, 
      IonicModule.forRoot(), 
      IonicStorageModule.forRoot(),
      AppRoutingModule,
      FormsModule, ReactiveFormsModule,
      AngularFireModule.initializeApp(environment.config),
      AngularFirestoreModule,
      AngularFireAuthModule,
      AngularFireStorageModule,
      HttpClientModule
    ],
  providers: [
    EnvServiceProvider,
    WebView,
    StatusBar,
    SplashScreen,
    Facebook,
    Storage,
    GooglePlus,
    NativeStorage,
    Camera,
    CameraPreview,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
