import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Router } from '@angular/router';
import { UsersService } from './shared/services/users.service';
import { Storage } from "@ionic/storage";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  constructor(  private platform: Platform,
                private splashScreen: SplashScreen,
                private statusBar: StatusBar,
                private userServ: UsersService,
                // private nativeStorage: NativeStorage,
                private nativeStorage: Storage,
                private router: Router,
  ){
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Check if this is the first time open
      this.nativeStorage.get('milestones_settings').then( dat => {
        if (dat && dat.appSlides) {
          this.nativeStorage.get('milestones_user').then( user => {
            if (user) {
              this.userServ.setUserFirebase(user);
              this.router.navigateByUrl('/tabs');
            }else{
              this.router.navigateByUrl('/login');
            }
          });
        }else{
          this.nativeStorage.set('milestones_settings',{ appSlides: true});
          this.router.navigateByUrl('/loading-screen');
        }
      });
      
      this.splashScreen.hide();
      this.statusBar.styleDefault();
    });
  }
}
