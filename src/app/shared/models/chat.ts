import { userDocument } from './users';
import { vendors } from './vendor';

export interface Chat {
    user           : string; //this.userServe.userId,
    vendor         : vendors; //this.VenSer.tempVendorId,
    vendorPackageId : string; //this.VenSer.tempVendorPackageId,
    eventId         : string; 
    is_seen?        : boolean;
    chatId?         : string; 
    createdAt       : string; //  new Date().getTime(),
    count?           : number; //0,
    messages?        : Array<Message>
}

export interface Message {
    uid             : string; // The Senders UID
    content         : string; // The Message Content
    createdAt       : string; // Time Sent
    isClient?        : boolean,
    isSeenByVendor?  : boolean,
    seenByVendorAt?  : string
    isVendor?        : boolean,
    isSeenByClient?  : boolean,
    seenByClientAt?  : string

    user?: any;
    vendor?:any;
}