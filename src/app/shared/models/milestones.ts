import { vendor_package } from './vendor'

export interface ms_event{
    address:	    string;     // The address of the event
    budget:         number;     // The budget for the event
    city:	        string;     // The city where the event will be held
    coordinatorId:  string;     // The ID of the coordinator if event is coordinator-managed
    description:    string;     // The description of the event
    eventId:	    string;     // The ID of the event
    eventType:	    string;     // The type of the event
    name:	        string;     // The name of the event
    maximumPax:     number;     // Maximum Pax
    packages?:      Array<event_packages>; // Vendor Packages
    selfManaged?:   boolean;    // The flag to determine if event is self-managed or coordinator-managed

    status:         string;     // Over all status of the event
    targetDateTime: string;     // Date Time in current milliseconds, The scheduled date and time of the event
    userId:         string;     // The ID of the currently logged user
    maxPax:     number      // The maximum number of participants

}

export interface event_packages{
    vendorId: string;
    vendorName:string;
    vendorPackage: vendor_package;
    vendorPackageId: string;

    type?:string;
    transactions?: Array<transactions>;
}

export interface ms_event_response {
    status: string;
    message?:string;
    coount: number
    body: ms_event;
}

export interface transactions{
    channel : string;
    eventId: string;
    paidAmount: number;
    paymentDate: any;
    referenceCode: string;
    transactionId: string;
    vendorPackageId: string;
    status?: string;
    receiptUrl?: string;

    // pay_channel: string;
    // pay_channel_name: string;
    // pay_status: boolean;
    // referenceCode: string;
    // paidAmount: number;
    // receiptDate: any;
}

