import { Observable } from 'rxjs';


export interface vendor_form{
    vendor_type: string;  
    vendor_id: string;
    vendor_name: string;
    vendor_status: string;
    vendor_payment: string;
    vendor_package_id: string;
}

export interface coordinator{
    // coordinatorId: string;
    // name: string;
    // age?: number;
    // imageRef?: string;
    // rating?:string;
    // vendors?: Array<coordPref>;
    // description?:string;
    type: string;
    coordinatorId: string;
    preferences: Array<coordPref>;
}

export interface coordPref{
    type: string;
    vendors: Array<vendorList>;
    // vendorIds: Array<string>;
}

export interface vendorList{
    vendorId: string;
    name?:string;
}
export interface response_enum{
    status: string;
    message?:string;
    count?:number;
    body: any;
}

export interface ventype_status{
    name:string;
    status: boolean;
}

export interface vendors {
    vendorId: string;
    vendorType?: vendor_type;
    name: string;
    description: string;
    address: string;
    cities: Array<string>;
    mailingAddress: string;
    email: string;
    contactPerson: string;
    contactNumber1: string;
    contactNumber2: string;
    mobileNumber: string;
    landlineNumber: string;
    taxId: string;
    businessPermitPhotoUrl: string;
    businessLogoPhotoUrl: string;
    businessBannerPhotoUrl: string;
    packageIds?: Array<string>;
    packages: Array<vendor_package>;
    pendingAmount?:number;
    coordinatorPreferences?: Array<coordPref>;

}

export interface vendor_type{
    name: string;
    subTypes?: Array<string>;
}

export interface package_type{
    type: string;
    subType?: string;
}

export interface vendor_package
{   
    vendorPackageId: string;
    name: string;
    description: string;
    maximumPax: number;
    photoUrl: string;
    discountRate: number;
    regularPrice: number;
    discountedPrice: number;
    vendorId?: string;
    vendorType?: vendor_type;
    packageType?: package_type;
  }