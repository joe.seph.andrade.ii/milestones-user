// export interface User {
//     isCreated?:any;

//     userId?: string;
//     firstName?: string;
//     lastName?: string;
//     address?: string;
//     sex?: string;
//     dob?:string;
//     email?: string;
//     mobileNumber?: any;

//     authToken?: any;
//     facebookId?: string;
//     googleid?: string;
//     imageUrl?: string;

//     selfieref?: string;
//     govtidfrontref?: string;
//     govtidbackref?: string;
// }

export interface userDocument{ 

    address: string;
    city: string;
    contactNumber1: string;
    contactNumber2: string;
    email: string;
    firstName: string;
    lastName: string;
    photoUrl: string;
    userId: string;
    
    birthday:string;
    gender:string;
    governmentIdUrl?:string;
    
    loginType: string; // Facebook, Google, Email
    authToken?: any;
    governmentIdBackUrl?:string;
    selfieUrl?:string;
    password?:string;
}