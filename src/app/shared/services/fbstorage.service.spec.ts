import { TestBed } from '@angular/core/testing';

import { FbstorageService } from './fbstorage.service';

describe('FbstorageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FbstorageService = TestBed.get(FbstorageService);
    expect(service).toBeTruthy();
  });
});
