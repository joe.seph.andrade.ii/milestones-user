import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { EnvServiceService } from "./env-service.service";
import { response_enum, vendor_package } from "../../shared/models/vendor";

@Injectable({
  providedIn: 'root'
})
export class VendorsService {

  private vendor_enum = new BehaviorSubject<string[]>(undefined);
  private fedration_url: string;
  private service_url: string;
  private vendors_url: string;

  private vendors = new BehaviorSubject([]);
  private packages = new BehaviorSubject([]);

  constructor( private httpClient: HttpClient,
               private envService: EnvServiceService) { 
    this.fedration_url = this.envService.getFederationUrl();
    this.service_url = this.envService.getEventsUrl();
    this.vendors_url = this.envService.getVendorsUrl();

    this.fetchVendorEnum();
  }


  async fetchVendorEnum() {
    await this.httpClient.get<response_enum>(this.fedration_url + this.envService["FedrationApiUrl"] + '/vendors/primary-types')
      .subscribe(data => {
        this.vendor_enum.next(data.body);
      });
  }

  fetchVendorsByType(vendorType: string) {
    this.httpClient.get<response_enum>(this.fedration_url + this.envService["FedrationApiUrl"] + '/vendors?vendorType=' + vendorType)
    .subscribe(data => {
      this.vendors.next(data.body);
    });
  }

  fetchVendorPackagesByType( vendorType: string, vendorId: string){
    if (vendorId) {
      this.httpClient.get<response_enum>(this.vendors_url + this.envService["VendorsApiUrl"] + '/vendors/' + vendorId + '/packages')
      .subscribe( data => {
        this.packages.next([data.body]);
      });
    }else{
      this.httpClient.get<response_enum>(this.vendors_url + this.envService["VendorsApiUrl"] + '/vendors/packages/?vendorType=' + vendorType)
      .subscribe( data => {
        this.packages.next(data.body);
      });
    }

  }

  getVendorEnum(){
    return this.vendor_enum.asObservable();
  }

  getVendors() {
    return this.vendors.asObservable();
  }

  getVendorPackages(){
    return this.packages;
  }
  
}
