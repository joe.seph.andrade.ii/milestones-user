import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { userDocument } from '../models/users';
import { BehaviorSubject, Observable } from 'rxjs';
import { EnvServiceService } from "./env-service.service";
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { LoadingController } from '@ionic/angular';
import { Facebook } from '@ionic-native/facebook/ngx';
import { Storage } from "@ionic/storage";
import { response_enum } from '../models/vendor';
import { UtilsService } from './utils.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private userDoc = new BehaviorSubject<userDocument>(undefined);
  private api_url: string;

  // These are used for the interceptor
  public token:string;
  public userId:string;
  
  constructor(private httpClient: HttpClient,
              private fireAuth: AngularFireAuth,
              private router: Router,
              private fb: Facebook,
              private loadCtr: LoadingController,
              // private nativeStorage: NativeStorage,
              private utilServ: UtilsService,
              private nativeStorage: Storage,
              private envService: EnvServiceService) { 

    this.api_url = this.envService.getFederationUrl();
  }

  isUserCreated() {
    // if (window.location.origin.includes("localhost")) {
    //   this.httpClient.get<User>(window.location.origin + '/assets/fakedata/user_data.json')
    //     .subscribe(data => {
    //       this.user.next(data);
    //     });
    // }
  }

  async fetchUserFirebase(userId: string) {
    this.userId = userId; // THIS IS CRITICAL TO HTTP REQUEST DO NOT REMOVE

    const loading = await this.loadCtr.create({
      message: 'Fetching your info...',
      spinner: 'bubbles',
      duration: 10000,
    });
    
    return new Promise((resolve,reject)=>{
      try {
        loading.present();
        console.log(userId);
        this.httpClient.get<userDocument>(`${this.api_url}${this.envService["FedrationApiUrl"]}/users/${userId}`)
                       .subscribe((res:any) => {
                          loading.dismiss();
                          console.log(res);
                          if (res.status != 'FAILED') {
                            this.saveUserStorage(res.data);
                            resolve(res.data);
                          }else{
                            reject(null);
                          }
                       });
        
      }catch (error) {
        console.log(error);
      }
    });
  }

  async createUserFirebase( ){
    const load = this.utilServ.showLoad('Finalizing your account', 60000);
    return new Promise<string>((resolve, reject)=>{
      try {
        this.userDoc.subscribe(userDoc=>{

          console.log(userDoc);
          this.httpClient.post<string>(this.api_url + this.envService["FedrationApiUrl"] + '/users', userDoc)
                         .subscribe(data => {
                           this.saveUserStorage(this.userDoc.value);
                           resolve(data);

                           load.then(dm=>{
                             dm.dismiss();
                           });

                         });
        }).unsubscribe();
        
      } catch (err) {
        load.then(dm=>{
          dm.dismiss();
        });
        reject(err);
      }
    });
  }

  getUserFirebase(): Observable<userDocument>{
    return this.userDoc.asObservable();
  }

  setUserFirebase(userDoc: userDocument){
    this.token = userDoc.authToken;
    this.userId = userDoc.userId;
    this.userDoc.next(userDoc);
  }

  async loginEmail(email, password){
    return new Promise((resolve, reject)=>{
      this.fireAuth.auth.signInWithEmailAndPassword(email,password).then((res)=>{
        
        res.user.getIdToken().then(token=>{
          this.token = token
        });

        this.userId = res.user.uid;
        this.fetchUserFirebase(res.user.uid).then(()=>{
          resolve();
        })
      }).catch(er=>{
        reject(er);
      })
    });
    
  }

  async createUser(email, password){
    const load = this.utilServ.showLoad('Setting up');
    
    return new Promise((resolve,reject)=>{
      this.fireAuth.auth.createUserWithEmailAndPassword(email, password).then((res)=>{
        load.then(dm=>{
          dm.dismiss();
        });

        resolve(res.user);
      }).catch((err)=>{

        load.then(dm=>{
          dm.dismiss();
        });

        reject(err);
      });
    });
  }


  saveUserStorage(data: userDocument){
    // this.token = data.authToken ? data.authToken : '';
    this.nativeStorage.set('milestones_user', data);
  }

  userLogout(){
    this.fireAuth.auth.signOut().then(() => {

      if (this.userDoc.value.loginType == 'facebook') {
        this.fb.logout();
      }

      this.userDoc.next(null);
      this.nativeStorage.remove('milestones_user');
      this.router.navigateByUrl('/login');
    });
  }
}
