import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ms_event, ms_event_response, event_packages, transactions } from '../models/milestones';
import { BehaviorSubject, Observable, Subscriber } from 'rxjs';
import { EnvServiceService } from "./env-service.service";
import { FormBuilder, FormGroup, Validators, FormControl, FormsModule, FormArray} from '@angular/forms';
import { vendor_package, vendors, response_enum, vendor_type, coordinator } from '../models/vendor';
import { UsersService } from './users.service';


@Injectable({
  providedIn: 'root'
})
export class MilestonesService {

  private event             = new BehaviorSubject<ms_event>(undefined);
  private ms_events         = new BehaviorSubject<ms_event[]>(undefined);
  private vendors_packages  = new BehaviorSubject<vendors[]>(undefined);
  private prefShow          = new BehaviorSubject<boolean>(undefined);
  
  public  milestonesForm: FormGroup;
  private service_url: string;
  private vendor_url: string;
  private coordinator = {} as coordinator;

  constructor(  private httpClient: HttpClient,
                private userServe: UsersService,
                private envService: EnvServiceService,
                private formBuilder: FormBuilder) { 
    
    this.service_url = this.envService.getEventsUrl();
    this.vendor_url = this.envService.getVendorsUrl();

    this.milestonesForm = formBuilder.group({
      step1Form: this.createStep1FG(),
      step2Form: [this.createStep2FG()]
    });
  }


  // Initializations
  createStep1FG(){
    return this.formBuilder.group({
      milestone_title:      ['', Validators.compose([Validators.maxLength(30), Validators.required])],
      milestone_type:       ['', Validators.compose([Validators.maxLength(50), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      milestone_budget:     ['', Validators.compose([Validators.maxLength(13), Validators.required])],
      milestone_city:       ['', Validators.compose([Validators.maxLength(30), Validators.required])],
      milestone_guestspax:  ['', Validators.compose([Validators.maxLength(6), Validators.required])],
      milestone_date:       ['', Validators.required],
    });
  }

  createStep2FG() {
    return this.formBuilder.group({
      vendor_type:  ['', Validators.required],
      vendor_id:    ['', Validators.required],
      vendor_name:  ['', Validators.required],
      vendor_status:['', Validators.required],
      vendor_package_id: ['', Validators.required],
    });
  }

  // Set Methods
  setEventInfo(event_info: ms_event){
    this.event.next(event_info);
  }

  // Set Selected Coordinator
  setCoordinator( coord : coordinator, prefShow: boolean){ 
    this.coordinator = coord;
    this.setPrefShow(prefShow);
  }

  setPrefShow(prefShow){
    this.prefShow.next(prefShow);
  }

  getPrefShow(){
    return this.prefShow.asObservable();
  }

  getCoordinator(){
    return this.coordinator;
  }

  // Fetch Methods

  async fetchEvents(){
    
    await this.httpClient.get<response_enum>(this.service_url + this.envService["EventsApiUrl"] + '/events?userId=' + this.userServe.userId)
    .subscribe(data => {
    
      let arr = {} as ms_event[];
      arr = data.body;
      console.log(data.body);
      arr.forEach(elem => {
        elem.packages.forEach( i => {
          i.vendorPackageId = i.vendorPackage.vendorPackageId;
        });
      })
      this.ms_events.next(arr);
    });
  }

  async fetchOneEvent(eventId){
    return new Promise<boolean>((resolve, reject) => {
      try {
        this.httpClient.get<response_enum>(this.service_url + this.envService["EventsApiUrl"] + '/events/' + eventId)
            .subscribe(data => {
              if (data.status == 'SUCCESS') {
                this.event.next(data.body);
                resolve(true);
              }
            });
      } catch (error) {
        reject(false);
      }
    });
    
  }

  async fetchVendorPackages(){
    await this.event.subscribe( data => {

      let vend : vendors[];
      vend = [];

      if (data.packages) {
        data.packages.forEach( vendor => {
          this.httpClient.get<response_enum>(this.vendor_url + this.envService["VendorsApiUrl"] + '/vendors/packages/' + vendor.vendorPackage.vendorPackageId)
          .subscribe(data => {
            vend.push(data.body);
          });
        });
  
        this.vendors_packages.next(vend);
      }
    });
  }

  // Get Methods
  getEventInfo(){
    return this.event.asObservable();
  }

  getEvents(){
    return this.ms_events.asObservable();
  }

  getVendorPackage(vendorType: string){

    const packages = new BehaviorSubject([]);
    this.vendors_packages.subscribe( data =>{
      data.forEach( elem =>{
        if ( elem.vendorType.name === vendorType ) { 
            packages.next([elem]);
          }
      });
    });

    return packages.asObservable();
  }

  getTempEvent(eventId: string) { 
    return new Promise<ms_event>((resolve, reject) => {
      try {
        this.httpClient.get<ms_event_response>(this.service_url + this.envService['EventsApiUrl'] + '/events/' + eventId)
        .subscribe(data => {
          if (data.status === 'SUCCESS') {
            resolve(data.body);
          }
        });
      } catch (error) {
        reject(false);
      }
    });
}


  // Add methods
  addVendorPackage(vendor: vendors, index: any){

    let temp = {} as vendors;
    temp.vendorId = vendor.vendorId;
    temp.name = vendor.name;
    temp.address = vendor.address;
    temp.vendorId = vendor.vendorId;
    temp.vendorType = vendor.vendorType;
    temp.description = vendor.description;
    temp.address = vendor.address;
    temp.cities = vendor.cities;
    temp.mailingAddress = vendor.mailingAddress;
    temp.email = vendor.email;
    temp.contactPerson = vendor.contactPerson;
    temp.contactNumber1 = vendor.contactNumber1;
    temp.contactNumber2 = vendor.contactNumber2;
    temp.mobileNumber = vendor.mobileNumber;
    temp.landlineNumber = vendor.landlineNumber;
    temp.taxId = vendor.taxId;
    temp.businessPermitPhotoUrl = vendor.businessPermitPhotoUrl;
    temp.businessLogoPhotoUrl = vendor.businessLogoPhotoUrl;
    temp.businessBannerPhotoUrl = vendor.businessBannerPhotoUrl;

    temp.packages = [];
    temp.packages.push(vendor.packages[index]) ;
    
    let object = [];
    if (this.vendors_packages.value != undefined) {
      object = this.vendors_packages.value;
    }
    
    object.push(temp);
    this.vendors_packages.next(object);
  }


  // Update Methods
  async updateEventPackages(){
    let array = [];

    if (this.vendors_packages.value.length > 0) {
      this.vendors_packages.subscribe( data => {
        data.forEach( elem => {
          elem.packages.forEach( value => {
  
              let event = {} as event_packages;
              // let pckg  = {} as event_packages;
  
              value.vendorId = elem.vendorId;
              value.name = elem.name;
  
              event.vendorId = elem.vendorId;
              event.vendorName = elem.name;
              event.vendorPackageId = value.vendorPackageId;
              event.vendorPackage = value;
              event.transactions= [];
              array.push(event);
          })
          
          this.event.value.packages = array;
  
        });
      });
    }else{
      this.event.value.packages = [];
    }

    
    try {
      await this.httpClient.put<response_enum>(this.service_url + this.envService["EventsApiUrl"] + '/events', this.event.value)
      .subscribe( data => {
        if (data.status === 'SUCCESS') {
          console.log(data.status);
        }
      })
    } catch (error) {
      console.log(error);
    }
  }


  async createMs(){
    
    let form1 = this.milestonesForm.controls['step1Form'].value;
    let msEvent = {} as ms_event;

    if (!this.event.value.selfManaged) {
      msEvent.coordinatorId = this.coordinator.coordinatorId;
    }
    msEvent.selfManaged = this.event.value.selfManaged;
    msEvent.description = form1.milestone_title;
    msEvent.status      = 'in_progress';
    msEvent.userId      = this.userServe.userId;
    msEvent.name        = form1.milestone_title;
    msEvent.eventType   = form1.milestone_type     
    msEvent.budget      = form1.milestone_budget;   
    msEvent.city        = form1.milestone_city;
    msEvent.packages    = [];
    msEvent.maximumPax  = form1.milestone_guestspax;
    msEvent.targetDateTime = new Date(form1.milestone_date).getTime().toString();     

    return new Promise<boolean>((resolve, reject) => {
      try {
        this.httpClient.post<response_enum>(this.service_url + this.envService["EventsApiUrl"] + '/events', msEvent)
        .subscribe( data => {
          if (data.status === 'SUCCESS') {
            this.event.next(data.body);
            resolve(true);
          }
        })
      } catch (error) {
        reject(false);
      }
    });
  }

  async deletePackage(v_package: vendor_package){


    return new Promise<boolean>((resolve, reject) => {
      try {
        let index: number;
        let items : any[] =  this.milestonesForm.controls["step2Form"].value;

        let found = items.find((data, idx) => {
          if (data.vendor_package_id == v_package.vendorPackageId) {
            index = idx;
          }
          return (data.vendor_package_id == v_package.vendorPackageId)
        });


        if (found && found.vendor_payment == 'Pending') {
          items.splice(index, 1);
          let ctr: number;
          let exist = this.vendors_packages.value.find((data, idx) => {
            if (found.vendor_id == data.vendorId) {
              ctr = idx;
            }
            return ( found.vendor_id == data.vendorId);
          });

          if (exist) {
            this.vendors_packages.value.splice(ctr, 1);
            this.updateEventPackages();
            resolve(true);
          }
        }
      } catch (error) {
        reject(false);
      }
    });
  }

  async updatePackage(v_idx: number, payment: transactions[]){
    
    if (!this.event.value.packages[v_idx].transactions) {
      this.event.value.packages[v_idx].transactions = [];
    }

    return new Promise<boolean>((resolve, reject) =>{
      try {

        payment.forEach( pay => {
          pay.eventId = this.event.value.eventId;
          if (pay.status == 'TQD') { // PENDING, VERIFIED, REJECTED
            pay.status = 'TPN';
          }
        });

        this.event.value.packages[v_idx].transactions = payment;
        this.httpClient.put<response_enum>(this.service_url + this.envService["EventsApiUrl"] + '/events', this.event.value)
        .subscribe( data => {
          if (data.status === 'SUCCESS') {
            this.fetchEventTransactions(this.event.value.eventId).then( res => {
              if (res) {
                let found = this.ms_events.value.find( event => {
                  return (event.eventId == res.eventId);
                });
                console.log(found);
                if (found) {
                  found.packages = res.packages;
                  resolve(true);
                }
                
              }
            });
          }else{
            reject(false);
          }
        });
      } catch (error) {
        reject(false);
      }
    });
  }

  async fetchEventTransactions(eventId){
    return new Promise<ms_event>((resolve, reject) => {
      try {
        this.httpClient.get<response_enum>(this.service_url + this.envService["EventsApiUrl"] + '/events/' + eventId)
            .subscribe(data => {
              if (data.status == 'SUCCESS') {
                resolve(data.body);
              }
            });
      } catch (error) {
        reject(null);
      }
    });
    
  }

  createStep2Form() {
    return this.formBuilder.group({
      vendor_type:  ['', Validators.required],
      vendor_id:    ['', Validators.required],
      vendor_name:  ['', Validators.required],
      vendor_status:['', Validators.required],
      vendor_payment:['', Validators.required],
      vendor_package_id: ['', Validators.required],
    });
  }

}
