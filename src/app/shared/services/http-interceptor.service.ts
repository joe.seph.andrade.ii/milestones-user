import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService  implements HttpInterceptor{

  constructor(private userServ: UsersService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
 
    const authToken = this.userServ.token;
    const userId = this.userServ.userId;

    if (!request.headers.has('Content-Type')) {
        request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
    }

    // request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
    request = request.clone({ headers: request.headers.set('Access-Control-Allow-Origin', '*') });
    request = request.clone({ headers: request.headers.set('X-User-ID', userId) });
    request = request.clone({ headers: request.headers.set('X-Authorization-Type', 'facebook') });
    request = request.clone({ headers: request.headers.set('X-Authorization', authToken) }); // Change this later to the actual Token

    return next.handle(request).pipe(
        map((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
                // console.log('event', event);
            }
            return event;
        }),
        // catchError((error: HttpErrorResponse) => {
        //     let data = {};
        //     data = {
        //         // domain: error.domain,
        //         message: error.message,
        //         // reason: error.reason
        //     };
        //     console.log(data);
        //     return throwError(error);
        // })
        );
  }
}
