import { AngularFireStorage, AngularFireUploadTask,} from '@angular/fire/storage';
import { from, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { switchMap } from 'rxjs/operators';


export interface FilesUploadMetadata {
  uploadProgress$: Observable<number>;
  downloadUrl$: Observable<string>;
}

@Injectable({
  providedIn: 'root'
})
export class FbstorageService {

  constructor(private readonly storage: AngularFireStorage) {
    
  }

  uploadFile( filePath: string, fileToUpload:any): FilesUploadMetadata {

    const customMetadata = { app: `Date Created: ${new Date().toISOString()}` };
    const { name } = fileToUpload;
    // const uploadTask: AngularFireUploadTask = this.storage.upload(filePath,fileToUpload);
    const uploadTask: AngularFireUploadTask = this.storage.ref(filePath).putString(fileToUpload,'data_url',{customMetadata});

    return {
      uploadProgress$: uploadTask.percentageChanges(),
      downloadUrl$: this.getDownloadUrl$(uploadTask, filePath),
    };

  }

  private getDownloadUrl$( uploadTask: AngularFireUploadTask, path: string ): Observable<string> {
    return from(uploadTask).pipe(
      switchMap((_) => this.storage.ref(path).getDownloadURL()),
    );
  }
}
