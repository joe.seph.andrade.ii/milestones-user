import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { MilestonesService } from './milestones.service';
import { map, flatMap, switchMap } from 'rxjs/operators';
import { firestore } from 'firebase/app';
import { UsersService } from './users.service';
import { VendorsService } from './vendors.service';
import { Chat, Message } from '../models/chat';
import { vendors } from '../models/vendor';
import { Observable, combineLatest, of, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(
    private afs: AngularFirestore,
    private router: Router,
    private userServe: UsersService,
    private VenSer: VendorsService,
    private milestones: MilestonesService
  ) {}

  getAllChat(){
    let temp = new BehaviorSubject<any>(undefined);
    let array = [];
    let sub = this.afs.collection<any>('chats', ref => 
        ref.where('user', '==', this.userServe.userId))
          .snapshotChanges()
          .subscribe(data=>{
            data.forEach( elem => {
              let chat = elem.payload.doc.data();
              if (chat.messages.length > 0) {

                let index = -1;
                let found = array.find( (fnd, idx) => {
                  if (fnd.chatId == chat.chatId) {
                    index = idx;
                  }
                  return (fnd.chatId == chat.chatId);
                });

                
                if (found && index > -1) {
                  array[index] = chat;
                }else{
                  array.push(elem.payload.doc.data());
                }
              }
            });
          });
    temp.next(array);
    return temp.asObservable();
  }

   //getChats
   get(chatId) {
    return this.afs
      .collection<any>('chats')
      .doc(chatId)
      .snapshotChanges()
      .pipe(
        map(doc => {
          return { id: doc.payload.id, ...doc.payload.data() as any };
        })
      );
  }

  getChats$(chatId) {
    let chat;
    const joinKeys = {};
    return this.afs
      .collection<any>('chats')
      .doc(chatId)
      .snapshotChanges()
      .pipe(
        map(doc => {
          return { id: doc.payload.id, ...doc.payload.data() as Chat };
        }),
        map((chat: Chat) => {
          let messages: Array<Message> = chat.messages;

          if(messages.length > 0 ) {
            for(let message of messages) {
              if(message.isClient) {
                message.user = chat.user
              }
  
              if(message.isVendor) {
                message.vendor = chat.vendor
              }
            }
          }
          return chat;
        })
      );
  }

  getChatItem(chat: Chat) {
    return new Promise<any>((resolve, reject) => {
      try {
        this.afs.collection('chats', 
        ref => 
          ref
             .where('user', '==', chat.user)
             .where('eventId', '==', chat.eventId)
             .where('vendorPackageId', '==', chat.vendorPackageId)
             .limit(1)
        )
        .valueChanges()
        .subscribe((query)=>{
          console.log(query);
          resolve(query[0])
        })
    } catch (error) {
      reject(false);
    }
  });
 
}

  checkChatExist(chat: Chat) {

    return new Promise<boolean>((resolve, reject) => {
      try {
        console.log(chat);
        this.afs.collection('chats', 
        ref => 
          ref.where('user', '==', chat.user)
             .where('eventId', '==', chat.eventId)
             .where('vendorPackageId', '==', chat.vendorPackageId)
             .limit(1)
        ).get()
        .subscribe((query)=>{
          if(query.size > 0) {
            resolve(true);
          } else {
            resolve(false);
          }
        })
    } catch (error) {
      reject(false);
    }
  });
 
  }

   // create/start a chat
  async isChatExist(vendor: vendors, chat: Chat) {

    return new Promise<boolean>(async (resolve, reject) => {
      try {
        let isChatExist = await this.checkChatExist(chat);
        if(isChatExist) {
          resolve(true)
        } else {
          resolve(false)
        }
    } catch (error) {
      reject(false);
    }

  });

   

}

  async createChat(chat: Chat) {
    return new Promise<any>(async (resolve, reject) => {
      try{
       chat.chatId = this.afs.createId();
       chat.messages = [];
       await this.afs.collection('chats').doc(chat.chatId).set(chat).then(()=>{
        this.afs.collection('chats').doc(chat.chatId).get()
        .subscribe((query)=>{
          if(query.exists) {
            resolve(query.data())
          } else {
            resolve(false)
          }
        })
       });
      }catch(error) {
        reject(false);
      }

    });

  }


  // create/start a chat
  // async create(vendor: vendors) {
  //   let eventId = "1fd8a51a-f2c5-46c8-81a3-47e1b0e1f974" // TEMP
  //   let msEvent = await this.milestones.getTempEvent(eventId) //TEMP

  //   const chat: Chat = { //create chat model
  //     userId: this.userServe.userId,
  //     vendorId: vendor.vendorId,
  //     vendorPackageId: vendor.packages[0].vendorPackageId,
  //     eventId: msEvent.eventId,
  //     chatId: this.afs.createId(),
  //     createdAt: new Date().getTime().toString(),
  //     count: 0,
  //     messages: []
  //   };

  //   let isChatExist = await this.checkChatExist(chat);
    
  //   if(isChatExist) {
  //     // console.log('getChat: ',await this.getChatItem(chat))
  //     let chatItem:Chat = await this.getChatItem(chat)
  //     this.goToPage('/chat/' + chatItem.chatId);    
  //     //get chat ID
  //     //go to chat page
  //   } else {

  //   }
   
   

  //   //check if chat is already exist


  //   // const docRef = await this.afs.collection('chats').doc(chat.chatId).set(chat)
  //   // console.log(docRef)
  //   // this.sendMessage(data, 'Hello Message')

  //   // const snapResult = await this.afs.collection('chats', 
  //   // ref => 
  //   //   ref.where('userId', '==', this.userServe.userId)
  //   //      .where('eventId', '==', msEvent.eventId)
  //   //      .where('vendorPackageId', '==', this.VenSer.tempVendorPackageId)
  //   // )
  //   // .snapshotChanges()
  //   // .pipe(switchMap(chats => chats)); 

    
  //   // snapResult.subscribe(doc => {
  //   //   cnt++;
  //   //   console.log('cnt ', cnt)
  //   //   console.log(doc.payload.doc.data())  
  //   // });
  //   // return this.router.navigate(['chats', docRef.id]);
  // }


  goToPage(page: string){
    this.router.navigate([page]);
  }

  getChatValue(source) {
    return new Promise<Chat>(async (resolve, reject) => {
      try{
        source.subscribe((chat: Chat)=>{
          resolve(chat)
        })
      } catch(error) {
        reject(false);
      } 
    });
  }    

  async updateMessage(chat$: Observable<any>) {
    let chat = await this.getChatValue(chat$)
    
    let messages = chat.messages.map((message: any)=>{
      
      if(message.isVendor && !message.isSeenByClient) {
        message.isSeenByClient = true;
        message.seenByClientAt = Date.now().toString()
      }
      return message;
    })

    let ctr = 0;
    if (chat.count) {
      ctr = chat.count + 1;
    }

    chat.messages = messages;
    const ref = this.afs.collection('chats').doc(chat.chatId);
    return ref.update({
      count: ctr,
      lastUpdate: new Date().getTime(),
      messages: chat.messages //firestore.FieldValue.arrayUnion(chat.messages)
    });
  }

  async sendMessage(chatId, content) {
    // const { uid } = await this.auth.getUser();

    console.log(this.userServe.userId);
      const data = {
        uid : this.userServe.userId,
        content,
        createdAt: Date.now(),
        isClient: true,
        isSeenByVendor: false,
        seenByVendorAt: null
      };

      const ref = this.afs.collection('chats').doc(chatId);
      return ref.update({
        is_seen: false,
        messages: firestore.FieldValue.arrayUnion(data)
      });
  }

  // joinUsers(chat$: Observable<any>, chatItem: Chat) {
  //   let chat;
  //   const joinKeys = {};
  //   // console.log('chatItem: ', chatItem)
  //   return chat$.pipe(
  //     switchMap(c => {
  //       // Unique User IDs
  //       chat = c;
  //       const uids = Array.from(new Set(c.messages.map(v => v.uid)));
  //       // console.log('uids: ', uids)

  //       // const userDocs = uids.map(u =>
  //       //   this.afs.doc(`users/${u}`).valueChanges()
  //       // );

  //       const userDocs = uids.map(u =>{
  //         if(chatItem.userId === u) {
  //           return this.afs.doc(`users/${u}`).valueChanges()
  //         } else if (chatItem.vendorId === u) {
  //           return this.afs.doc(`vendors/${u}`).valueChanges()
  //         }
  //        }
  //       );

  //       return userDocs.length ? combineLatest(userDocs) : of([]);
  //     }),
  //     map(arr => {
  //       // console.log('arr', arr)
  //       // arr.forEach(v => (joinKeys[(<any>v).userId] = v));
  //       // chat.messages = chat.messages.map(v => {
  //       //   return { ...v, user: joinKeys[v.uid] };
  //       // });
  //       // return chat;
        
  //       arr.forEach((v:any) => {
  //         // console.log('v', v)
  //         if(chatItem.userId === v.userId) {
  //           joinKeys[(<any>v).userId] = v
  //         } else if(chatItem.vendorId === v.vendorId) {
  //           joinKeys[(<any>v).vendorId] = v
  //         }
  //       });
  
  //       // console.log('joinkeys: ', joinKeys)
  //       chat.messages = chat.messages.map(v => {
  //         return { ...v, 
  //           user: chatItem.userId === v.uid ? joinKeys[v.uid]: false,
  //           vendor: chatItem.vendorId === v.uid ? joinKeys[v.uid]: false,
  //         };
  //       });
  //       return chat;
  //     })
  //   );
  // }


}
