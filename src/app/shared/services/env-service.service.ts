import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnvServiceService {

  // Federation API url
  public FedrationApiUrl = '/federation-api';

  // Events Service api Url
  public EventsApiUrl = '/events-api';

  public VendorsApiUrl = '/packages-api'

  

  // Whether or not to enable debug mode
  public enableDebug = true;

  constructor() {
  }

  public getFederationUrl(): string {
    // return 'http://localhost:8080';
    // return 'http://milestones-federation.ap-southeast-1.elasticbeanstalk.com';
    return 'https://milestones-federation-service.herokuapp.com';
  }

  public getEventsUrl(): string {
    return 'https://milestones-events-service.herokuapp.com';
  }

  public getVendorsUrl(): string{
    return 'https://package-management-service.herokuapp.com';
  }
}
