import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlldealsPage } from './alldeals.page';

describe('AlldealsPage', () => {
  let component: AlldealsPage;
  let fixture: ComponentFixture<AlldealsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlldealsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlldealsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
