import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AllmerchantsPage } from './allmerchants.page';

const routes: Routes = [
  {
    path: '',
    component: AllmerchantsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AllmerchantsPage]
})
export class AllmerchantsPageModule {}
