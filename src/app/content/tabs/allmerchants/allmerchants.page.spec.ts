import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllmerchantsPage } from './allmerchants.page';

describe('AllmerchantsPage', () => {
  let component: AllmerchantsPage;
  let fixture: ComponentFixture<AllmerchantsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllmerchantsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllmerchantsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
