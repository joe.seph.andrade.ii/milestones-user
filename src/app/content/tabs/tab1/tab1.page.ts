import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, IonSlides } from '@ionic/angular';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-tab1',
  templateUrl: './tab1.page.html',
  styleUrls: ['./tab1.page.scss'],
})

export class Tab1Page implements OnInit {


    @ViewChild('sliderRef', { static: true }) slider: IonSlides;

    public timestart: any;
    public img_placeholder : string = "../../../../assets/images/placeholder-square.jpg";

    sliderOpts = {
        initialSlide: 0,
        effect: 'flip',
        speed: 500
    }

  constructor(private router: Router) { 

  }

  ngOnInit() {
  }

  goToPage(){
    this.router.navigateByUrl('/promo-details');
  }

  openProfile(){
    this.router.navigate(["/profile"]);
  }
}
