import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/shared/services/users.service';
import { AlertController } from '@ionic/angular';



@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
  public flag1 : boolean = true;

  constructor( private router: Router,
               private userServ: UsersService,
               private alertCtr: AlertController,
              ){ 

  }

  ngOnInit() {
    
  }

  ionViewDidEnter(){
    this.flag1 = true;
  }

  toggleToolBar(){
    this.flag1 = !this.flag1;
  }

  goToCreate(){
    this.flag1 = true;
    this.router.navigate(["/msForm"]);

  }

  goToPage(page: string){
    this.router.navigate([page]);
  }

  async signOut(){
    this.flag1 = !this.flag1;

    const alert = await this.alertCtr.create({
      header: 'Confirm',
      message: 'Proceed logout?',
      buttons: [
        {
          text: 'cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'logout',
          handler: () => {
            this.userServ.userLogout();
          }
        }
      ]
    });
  
    await alert.present();

    
  }

}
