import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TabsPage } from "./tabs.page"

const routes: Routes = [
    {
        path: 'tabs',
        component: TabsPage,
        children: [
            // Tab 1 Routing
            {
                path: 'tab1',
                children: [
                    {
                        path: '',
                        loadChildren: './tab1/tab1.module#Tab1PageModule'
                    },
                ]
            },
            // Tab 2 Routing
            {
                path: 'view-events',
                children: [
                    {
                        path: '',
                        loadChildren: './view-events/view-events.module#ViewEventsPageModule'
                    }
                ]
            },
            // Tab 3 Routing
            {
                path: 'tab3',
                children: [
                    {
                        path: '',
                        loadChildren: './tab3/tab3.module#Tab3PageModule'
                    }
                ]
            },
            // Tab 4 Routing
            {
                path: 'tab4',
                children: [
                    {
                        path: '',
                        loadChildren: './tab4/tab4.module#Tab4PageModule'
                    }
                ]
            },
            // Tab 5 Routing
            {
                path: 'tab5',
                children: [
                    {
                        path: '',
                        loadChildren: './tab5/tab5.module#Tab5PageModule'
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/tabs/tab1',
                pathMatch: 'full',
            }
        ]
    },

    {
        path: '',
        redirectTo: '/tabs/tabs/tab1',
        pathMatch: 'full',
    }
];

@NgModule({
    imports:[
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule] 
})
export class TabsPageRoutingModule {}