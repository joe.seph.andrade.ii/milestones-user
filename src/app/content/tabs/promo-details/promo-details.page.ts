import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-promo-details',
  templateUrl: './promo-details.page.html',
  styleUrls: ['./promo-details.page.scss'],
})
export class PromoDetailsPage implements OnInit {

  constructor( private navCtr: NavController) { }

  ngOnInit() {
  }

  navBack(){
    this.navCtr.pop();
  }

}
