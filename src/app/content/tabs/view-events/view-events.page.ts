import { Component, OnInit, OnDestroy} from '@angular/core';
import { MilestonesService } from 'src/app/shared/services/milestones.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { ms_event } from 'src/app/shared/models/milestones';
import { Router } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { Subscription } from "rxjs";

@Component({
  selector: 'app-view-events',
  templateUrl: './view-events.page.html',
  styleUrls: ['./view-events.page.scss'],
})
export class ViewEventsPage implements OnInit, OnDestroy {

  public state : Subscription;

  public events = new Observable<ms_event[]>();
  constructor( private msServ: MilestonesService,
               private loadCtr: LoadingController,
               private alertCtr: AlertController,
               private router: Router) { 
    this.msServ.fetchEvents();
  }

  doRefresh(event) {
    
    this.msServ.fetchEvents();
    this.events = this.msServ.getEvents();

    setTimeout(() => {
      event.target.complete();
    }, 2000);

    return true;
  }

  ngOnDestroy(): void {
    this.state.unsubscribe();
  }

  async ngOnInit() {

    await this.loadCtr.create({
      message: 'Fetching your created milestones!',
      duration: 10000
    }).then( res => {
      res.present();
      res.onDidDismiss().then( data => {
        if (data.role != 'success') {
          this.presentAlert();
        }
      });
    });

    this.events = this.msServ.getEvents();
    this.state = this.events.subscribe( data => {
      if (data != undefined) {
        this.loadCtr.getTop().then( res => {
          if (res != undefined) {
            this.loadCtr.dismiss(data,'success');
          }
        })
      }
    });
  }

  async presentAlert() {
    const alert = await this.alertCtr.create({
      header: 'Ooops!',
      message: "You have no events created yet!",
      buttons: ['OK']
    });

    await alert.present();
  }

  async viewEvent( event: ms_event){

    await this.loadCtr.create({
      message: 'one sec...',
      duration: 1000
    }).then( res => {
      res.present().then(a=>{
        this.msServ.setEventInfo(event);
      });
      res.onDidDismiss().then(a => {
        this.router.navigateByUrl('/msChecklist');
      })
    });
    
    
  }

}
