import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewEventsPage } from './view-events.page';

describe('ViewEventsPage', () => {
  let component: ViewEventsPage;
  let fixture: ComponentFixture<ViewEventsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewEventsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewEventsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
