import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatService } from 'src/app/shared/services/chat.service';
import { Observable, Subscription } from 'rxjs';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { MilestonesService } from 'src/app/shared/services/milestones.service';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit, OnDestroy {

  public chatList = [];
  private subs : Subscription;
  constructor( private chatServ: ChatService,
               private loadCtr: LoadingController,
               private msServ: MilestonesService,
               private router: Router,
               private chtServ: ChatService) { 
    
  }

  ngOnInit() {
    this.getChats();
  }

  ngOnDestroy(){
    this.subs.unsubscribe();
  }

  getChats(){
   let list : Observable<any>;

    list = this.chatServ.getAllChat();
    this.subs = list.subscribe(data => {
      this.chatList = [];
      this.chatList = data;
      console.log(this.chatList);
    });
  }

  public get sortedArray(): any {
    return this.chatList.sort((a, b) => b.lastUpdate - a.lastUpdate);
}

  openChat(chat, idx){
    
    this.loadCtr.create({
      message: 'Checking messages...',
      spinner: 'dots',
      duration: 10000,
    }).then(async (resp) => {
        resp.present();
        let isChatExist =  await this.chtServ.isChatExist(chat.vendorId, chat);
        if (isChatExist) {
          let chatItem = await this.chtServ.getChatItem(chat)
          this.msServ.fetchOneEvent(chat.eventId).then( res => {
            if (res) {
              if(isChatExist && event) { 
                resp.dismiss();
                this.router.navigateByUrl('/chat/' + chatItem.chatId + '/' + idx + '/home' );
              }else{
                console.log('Event/Chat does not exist!');
              }
            }
          });
        }
        
    });
  }

  doRefresh(event) {

    this.getChats();

    setTimeout(() => {
      event.target.complete();
    }, 2000);

    return true;
  }


  newMsg( chat: any){
    if (chat.isSeenByClient != undefined && !chat) {
      return 'ms-new-message';
    }
  }

}
