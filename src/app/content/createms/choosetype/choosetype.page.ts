import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, IonSlides, LoadingController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { ms_event } from 'src/app/shared/models/milestones';
import { MilestonesService } from "src/app/shared/services/milestones.service";

@Component({
  selector: 'app-choosetype',
  templateUrl: './choosetype.page.html',
  styleUrls: ['./choosetype.page.scss'],
})
export class ChoosetypePage implements OnInit {

  @ViewChild('sliderRef',{static: true}) slider: IonSlides;

  sliderOpts = {
    initialSlide: 0,
    effect: 'flip',
    speed: 500
  };

  private event : Observable<ms_event>;
  constructor( private navCtr: NavController,
               private loadCtr: LoadingController,
               private toastCtr: ToastController,
               private msServ: MilestonesService) { }

  ngOnInit() {
    this.event = this.msServ.getEventInfo();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    
  }

  goToPage(page: string){
    let response: boolean = false;

    this.slider.getActiveIndex().then( value => {
      switch (value) {  
        case 0:
          let ctr = 0;
          let sub = this.event.subscribe(data => {
            if (ctr == 0) {
              ctr++;
              data.selfManaged = true;
              this.msServ.setEventInfo(data);
            }
          });

          this.loadCtr.create({
            message: 'Uploading your event to cloud ...',
            spinner: 'dots',
          }).then((res) => {
            res.present();

            this.msServ.createMs().then( status => {
              if (status) {
                response = true;
                sub.unsubscribe();
                this.presentToast('Milestone event draft saved!','success');
                this.navCtr.navigateForward(['/msChecklist']);  
              }else{
                this.presentToast('Saving failed, please check connections.','medium');
              }
              this.loadCtr.dismiss();
            })

            res.onDidDismiss().then(() => {  
              if (!response) {
                this.presentToast('Saving failed, please check your connections.','warning');
              }        
            });
          });

          
          break;
      
        case 1:
          this.navCtr.navigateForward(['/choosecoordinator']);
          break;
      }
    });
  }

  async presentToast(msg: string, type: string) {
    const toast = await this.toastCtr.create({
      message: msg,
      duration: 2000,
      position: 'top',
      color: type,
    });
    toast.present();
  }

}
