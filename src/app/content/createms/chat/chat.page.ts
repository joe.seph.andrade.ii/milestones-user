import { Component, OnInit, NgZone, ViewChild, OnDestroy } from '@angular/core';
import { ChatService } from 'src/app/shared/services/chat.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { tap,map } from 'rxjs/operators';

import { vendor_form, vendors } from 'src/app/shared/models/vendor';
import { MilestonesService } from 'src/app/shared/services/milestones.service';
import { ms_event } from 'src/app/shared/models/milestones';
import { NavController } from '@ionic/angular';
import { Chat, Message } from 'src/app/shared/models/chat';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit, OnDestroy {
  @ViewChild('content', {static: true}) private content: any;
  public chatId: any;
  private source: any;
  public chat$: Observable<any>;
  public chat = {} as Chat;
  public newMsg: string;

  private subs : Array<Subscription> = [];

  // private chat$ = new BehaviorSubject<Chat[]>([]);
  public event = {} as ms_event;

  public index: any;
  public item = {} as vendor_form;
  public vendors : Observable<vendors[]>;
  private isFirstOpen: boolean = true;
  public disableInfiniteScroll: boolean = true;
  private messageSize: number = 0;
  private messages: Array<Message> = [];

  limit = 0;
  constructor(
    private navCtr: NavController,
    private msServ: MilestonesService,
    private route: ActivatedRoute,
    private router: Router,
    private chtServ: ChatService,
    private ngZone: NgZone
  ) { 


  }

  ngOnDestroy(){
    this.subs.forEach( sub => {
      sub.unsubscribe();
    })
  }
  async ngOnInit() {
    let sub1 = this.route.params.subscribe(async (idx)=>{
      this.chatId = idx.chatId;   
      this.source = idx.source;
      this.getChats();
    });


    let sub2 = this.msServ.getEventInfo().subscribe(event=>{
      this.event = event;
    });

    this.subs.push(sub1, sub2);
  }

  async getChats() {
    let sub3 = this.chtServ.getChats$(this.chatId)
    .pipe(
      map((chat: Chat) =>{

        let c:Chat = chat;
        this.disableInfiniteScroll = false;

        this.messageSize = c.messages.length;

        this.messages = [];
        Array.prototype.push.apply( this.messages, c.messages);
        
        c.messages =  c.messages.sort(function(a:any,b:any){
          return b.createdAt - a.createdAt;
        })

        c.messages = c.messages.splice(0, (20 + this.limit))
        c.messages.reverse();
      
        chat.messages = c.messages;

        if(this.isFirstOpen) {
          this.isFirstOpen = false;
          this.disableInfiniteScroll = false
        }
  
        this.content.scrollToBottom(0);
        return chat;
      })
    ).subscribe((data)=>{
      this.chat = data
    });

    this.subs.push(sub3);
  }

  async doRefresh(event) {
    
    setTimeout(() => {
      this.limit = this.limit + 20;
      let _messages: Array<Message> = [];
      Array.prototype.push.apply( _messages, this.messages);
  
      _messages =  _messages.sort(function(a:any,b:any){
        return b.createdAt - a.createdAt;
      })
  
      _messages = _messages.splice(0, (20 + this.limit))
      _messages.reverse();
      
      this.chat.messages = _messages;
  
      if(this.messageSize <= this.limit) {
        this.disableInfiniteScroll = true;
      }

      event.target.complete();
    }, 1000);
  }

  async focusFunction() {
    let source = this.chtServ.get(this.chatId)
    this.chtServ.updateMessage(source)
  }

  submit() {
    if (this.newMsg != '') {
      this.chtServ.sendMessage(this.chatId, this.newMsg);
      this.newMsg = '';
      this.content.scrollToBottom(300);
    }
  }

  goBackToPage(){
    switch (this.source) {
      case 'home':
        this.router.navigate(["/tabs/tabs/tab4"]);
        break;

      case 'details':
        this.navCtr.navigateBack('/msChecklist');
        break;
    
    }
    
  }

  trackByCreated(i, msg) {
    return msg.createdAt;
  } 
 
  

  sleep(millis) {
    return new Promise<boolean>(async (resolve, reject) => {
      setTimeout(()=>{
        this.getChats()
        resolve(true)
      },millis)
    });
  } 

}
