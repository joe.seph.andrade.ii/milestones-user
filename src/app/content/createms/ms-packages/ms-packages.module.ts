import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ModalDetailsPage } from "./modal-details/modal-details.page";

import { IonicModule } from '@ionic/angular';

import { MsPackagesPage } from './ms-packages.page';

const routes: Routes = [
  {
    path: '',
    component: MsPackagesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MsPackagesPage, ModalDetailsPage],
  entryComponents: [ ModalDetailsPage ],
})
export class MsPackagesPageModule {}
