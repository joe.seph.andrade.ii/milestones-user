import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MilestonesService } from 'src/app/shared/services/milestones.service';
import { ms_event } from 'src/app/shared/models/milestones';
import { VendorsService } from 'src/app/shared/services/vendors.service';
import { NavController, ModalController, LoadingController, AlertController } from '@ionic/angular';
import { ModalDetailsPage } from "./modal-details/modal-details.page";
import { vendors, vendor_type } from 'src/app/shared/models/vendor';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-ms-packages',
  templateUrl: './ms-packages.page.html',
  styleUrls: ['./ms-packages.page.scss'],
})
export class MsPackagesPage implements OnInit, OnDestroy {

  public event = {} as ms_event;
  public vendors = [];
  public vendorType= {} as vendor_type;
  public isItemAvailable;
  private index: string;  
  private subs: Array<Subscription> = [];

  constructor(private route : ActivatedRoute,
              private modCtr: ModalController,
              private msServ: MilestonesService,
              private VenSer: VendorsService,
              private router: Router,
              private loadCtr: LoadingController,
              private navCtr: NavController,
              private alertCtr: AlertController) { 
    
  }

  ngOnDestroy(){
    this.vendors = [];
    this.subs.forEach( sub => {
      sub.unsubscribe();
    });
  }

  ionViewDidLeave(){
    this.vendors = [];
  }

  async ngOnInit() {

    this.vendors = [];

    await this.loadCtr.create({
      message: 'Fetching packages...',
      // duration: 5000
    }).then( res => {
      res.present();
      res.onDidDismiss().then( data => {
        if (data.role != 'success') {
          this.presentAlert();
        }
      });
    });

    let sub1  = await this.route.params.subscribe(idx=>{
      this.index = idx.idx
      let val = this.msServ.milestonesForm.controls['step2Form'].get(this.index).value;
      this.vendorType.name = val.vendor_type;
      this.VenSer.fetchVendorPackagesByType(val.vendor_type, val.vendor_id);     
    });

    let sub2 = this.msServ.getEventInfo().subscribe(event=>{
      if (event != undefined) {
        this.event = event;
        let top = this.loadCtr.getTop();
        if (top) {
          top.then( res => {
            sub2.unsubscribe();
            res.dismiss( event, 'success');
          });
          
        }
      }
    });

    this.subs.push(sub1, sub2);
    
  }

  async presentAlert() {
    const alert = await this.alertCtr.create({
      header: 'Ooops!',
      message: "Something went wrong, please check your internet connection.",
      buttons: ['OK']
    });

    await alert.present();
  }

  ionViewDidEnter(){
    this.vendors = [];
    this.subscribers();
  }



  async openModal(data: vendors, idx : any) {
    const modal = await this.modCtr.create({
      component: ModalDetailsPage,
      componentProps: {
        'vendor': data,
        'index': idx
      }
    });
 
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.role == 'select') {
        this.msServ.milestonesForm.controls['step2Form'].get(this.index).patchValue({
          vendor_type: data.vendorType.name,
          vendor_id: data.vendorId,
          vendor_name: data.name,
          vendor_status: false,
          vendor_payment: 'Pending',
          vendor_package_id: data.packages[idx].vendorPackageId,
        });

        this.msServ.addVendorPackage(data, idx);
        this.msServ.updateEventPackages();
        this.router.navigateByUrl('msItemDetails/' + this.index);
      }
    });
    
    return await modal.present();
  }

  subscribers() {
    this.vendors = null;
    this.vendors = [];

    let sub3 =  this.VenSer.getVendorPackages().subscribe((data)=>{
      if(Object.keys(data).length > 0) {
          this.vendors = data;
      }
    });

    this.subs.push(sub3);
  }

  getItems(ev:any){
    // Reset items back to all of the items
    this.subscribers();
    // set val to the value of the searchbar
    const val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.isItemAvailable = true;
      this.vendors = this.vendors.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }


  selectVendor(vendor) {
    
    // this.msServ.milestonesForm.controls['step2Form'].get(this.index).patchValue({
    //   vendor_id: vendor.vendorId,
    //   vendor_name: vendor.name,
    // });

    // this.back();
    this.router.navigate(['/msItemDetails']);
  }

  goToPage(page: string){
    this.navCtr.navigateBack([page]);
  }

  toggleClass(type: string = ''){
    
    switch (type) {
      case 'DECORS':
        return 'alert alert-warning';
        break;

      case 'FOOD CATERING':
        return 'alert alert-primary';
        break;

      case 'FOOD DESSERTS':
        return 'alert alert-success';
        break;

      default:
        return 'alert alert-warning';
        break;
    }
  }

  toggleIcon(type: string = ''){
    switch (type) {
      case 'DECORS':
        return 'fas fa-utensils';
        break;

      case 'FOOD CATERING':
        return 'fas fa-fan';
        break;

      case 'FOOD DESSERTS':
        return 'fas fa-map-marker-alt';
        break;
      
      default:
        return 'fas fa-splotch'
      break;
    }
  }

  toggleStatus(status: boolean){
    if (!status) {
      return 'fas fa-plus';
    }else{
      return 'fas fa-check';
    }
  }

  back(){
    this.navCtr.navigateBack(['/msChecklist']);
  }

}
 