import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, NavParams } from '@ionic/angular';
import { vendors } from "src/app/shared/models/vendor";
import { MilestonesService } from 'src/app/shared/services/milestones.service';

@Component({
  selector: 'app-modal-details',
  templateUrl: './modal-details.page.html',
  styleUrls: ['./modal-details.page.scss'],
})

export class ModalDetailsPage implements OnInit {

  public vendor = {} as vendors;
  public idx : any;

  constructor( private modCtr: ModalController,
               private msServ: MilestonesService,
               private navParam: NavParams,
               private navCtr: NavController) { 
    this.vendor = this.navParam.get('vendor');
    this.idx = this.navParam.get('index');
  }

  
  ngOnInit() {
  }



  dismiss(){
    this.modCtr.dismiss({'vendor':'test'},'cancel');
  }

  select(){
    this.modCtr.dismiss({},'select');
  }

}
