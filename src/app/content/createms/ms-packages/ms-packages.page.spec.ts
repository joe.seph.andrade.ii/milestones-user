import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MsPackagesPage } from './ms-packages.page';

describe('MsPackagesPage', () => {
  let component: MsPackagesPage;
  let fixture: ComponentFixture<MsPackagesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MsPackagesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MsPackagesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
