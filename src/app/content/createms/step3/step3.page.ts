import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MilestonesService } from 'src/app/shared/services/milestones.service';
import { ms_event } from 'src/app/shared/models/milestones';
import { VendorsService } from 'src/app/shared/services/vendors.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-step3',
  templateUrl: './step3.page.html',
  styleUrls: ['./step3.page.scss'],
})
export class Step3Page implements OnInit {
  
  public event = {} as ms_event;
  public vendors = [];
  public vendorType:string;
  public isItemAvailable;

  constructor( 
    private route : ActivatedRoute,
    private msServ: MilestonesService,
    private VenSer: VendorsService,
    private router: Router,
    private navCtr: NavController,
    ) { }

  index: string;  
  ngOnInit() {

    this.vendors = [];

    this.route.params.subscribe(idx=>{
      this.index = idx.idx

      this.vendorType = this.msServ.milestonesForm.controls['step2Form'].get(this.index).value.vendor_type;
       this.VenSer.fetchVendorsByType(this.vendorType);       
    });

    this.msServ.getEventInfo().subscribe(event=>{
      this.event = event;
    });
  }

  ionViewDidEnter(){
    this.subscribers();
  }

  subscribers() {
    this.vendors = [];
    this.VenSer.getVendors().subscribe((data)=>{
      if(Object.keys(data).length > 0) {
          this.vendors = data;
      }
    })
  }

  getItems(ev:any){
    // Reset items back to all of the items
    this.subscribers();
    // set val to the value of the searchbar
    const val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.isItemAvailable = true;
      this.vendors = this.vendors.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }


  selectVendor(vendor) {
    
    this.msServ.milestonesForm.controls['step2Form'].get(this.index).patchValue({
      vendor_id: vendor.vendorId,
      vendor_name: vendor.name,
    });

    this.goToPage('/step2');
  }

  goToPage(page: string){
    this.navCtr.navigateBack([page]);
  }

  toggleClass(type: string = ''){
    
    switch (type) {
      case 'DECORS':
        return 'alert alert-warning';
        break;

      case 'FOOD CATERING':
        return 'alert alert-primary';
        break;

      case 'FOOD DESSERTS':
        return 'alert alert-success';
        break;

      default:
        return 'alert alert-warning';
        break;
    }
  }

  toggleIcon(type: string = ''){
    switch (type) {
      case 'DECORS':
        return 'fas fa-utensils';
        break;

      case 'FOOD CATERING':
        return 'fas fa-fan';
        break;

      case 'FOOD DESSERTS':
        return 'fas fa-map-marker-alt';
        break;
      
      default:
        return 'fas fa-splotch'
      break;
    }
  }

  toggleStatus(status: boolean){
    if (!status) {
      return 'fas fa-plus';
    }else{
      return 'fas fa-check';
    }
  }

  openModal(){

  }
  

}
