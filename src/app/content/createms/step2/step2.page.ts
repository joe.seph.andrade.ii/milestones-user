// import { Component, OnInit } from '@angular/core';
// import { ModalController } from '@ionic/angular';
// // import { ItemsmodalPage } from "./itemsmodal/itemsmodal.page"
// import { MilestonesService } from "../../../shared/services/milestones.service";
// import { VendorsService } from "../../../shared/services/vendors.service";
// import { ms_event } from "../../../shared/models/milestones";
// import { ventype_status } from "../../../shared/models/vendor";
// import { Router } from '@angular/router';
// import { BehaviorSubject, Observable } from 'rxjs';
// import { AbstractControl, FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

// @Component({
//   selector: 'app-step2',
//   templateUrl: './step2.page.html',
//   styleUrls: ['./step2.page.scss'],
// })

// export class Step2Page implements OnInit {
//   private dataReturned:any;
//   public event = {} as ms_event;
//   public vendors = new BehaviorSubject<string[]>(undefined);
//   // public arrayTypes: Array<string> = [];
//   public arrayTypes: Array<ventype_status> = [];

//   public  step2Form: FormArray;
//   public  itemForm: FormGroup;
//   public  formArrays = [];

//   constructor( private modalController: ModalController,
//                private router: Router,
//                private formBuilder: FormBuilder,
//                private msServ: MilestonesService,
//                private VenSer: VendorsService,
//               ) { 

//     // this.VenSer.fetchVendorEnum();

//     // this.event.name = "Clifford's Birthday";
//     // this.event.budget = 100000;
//     // this.event.targetDateTime = 1571762180944;

//     // this.itemForm = formBuilder.group({
//     //   vendor_type:  ['', Validators.required],
//     //   vendor_id:    ['', Validators.required],
//     //   vendor_name:  ['', Validators.required],
//     //   vendor_status:['', Validators.required]
//     // })
//   }

//   // createStep2Form() {
//   //   return this.formBuilder.group({
//   //     vendor_type:  ['', Validators.required],
//   //     vendor_id:    ['', Validators.required],
//   //     vendor_name:  ['', Validators.required],
//   //     vendor_status:['', Validators.required],
//   //   });
//   // }

//   ngOnInit() {
//     // this.msServ.getEventInfo().subscribe(event=>{
//     //   this.event = event;
//     // });

//     this.msServ.setEventInfo(this.event);
//   }


//   // toggleClass(type: string){
    
//   //   switch (type) {
//   //     case 'DECORS':
//   //       return 'alert alert-warning';
//   //       break;

//   //     case 'FOOD':
//   //       return 'alert alert-primary';
//   //       break;

//   //     case 'FURNITURE':
//   //       return 'alert alert-success';
//   //       break;

//   //     default:
//   //       return 'alert alert-warning';
//   //       break;
//   //   }
//   // }

//   // toggleIcon(type: string){
//   //   switch (type) {
//   //     case 'DECORS':
//   //       return 'fas fa-fan';
//   //       break;

//   //     case 'FOOD':
//   //       return 'fas fa-utensils';
//   //       break;

//   //     case 'FURNITURE':
//   //       return 'fas fa-chair';
//   //       break;
      
//   //     default:
//   //       return 'fas fa-splotch'
//   //     break;
//   //   }
//   // }

//   // toggleStatus(status: boolean){
//   //   if (!status) {
//   //     return 'fas fa-plus';
//   //   }else{
//   //     return 'fas fa-check';
//   //   }
//   // }

//   // async openModal() {
//   //   const modal = await this.modalController.create({
//   //     component: ItemsmodalPage,
//   //     componentProps: {
//   //      "items": this.formArrays,
//   //     }
//   //   });
 
//   //   modal.onDidDismiss().then((dataReturned) => {
      

//   //     if (dataReturned != null) {
//   //       let _formOthers = this.createStep2Form();
//   //       if (!dataReturned.data) {
//   //         _formOthers.patchValue({
//   //           vendor_type: 'OTHERS',
//   //           vendor_id: null,
//   //           vendor_name: '',
//   //           vendor_status: false,
//   //         });
//   //         this.formArrays.push(_formOthers);
//   //         this.step2Form = this.formBuilder.array(this.formArrays);
          
//   //       }else{
//   //         this.dataReturned = dataReturned.data;
//   //         Object.keys(this.dataReturned).forEach(data=>{
//   //           let _form = this.createStep2Form();
//   //           if (this.dataReturned[data]) {
//   //             _form.patchValue({
//   //               vendor_type: data,
//   //               vendor_id: null,
//   //               vendor_name: '',
//   //               vendor_status: false,
//   //             });
              
//   //             this.formArrays.push(_form);
//   //           }
//   //         });
//   //         this.step2Form = this.formBuilder.array(this.formArrays);
//   //       }
//   //       this.msServ.milestonesForm.setControl('step2Form', this.step2Form);
//   //     }
//   //     console.log(this.msServ.milestonesForm.controls['step2Form']);
//   //   });
    
//   //   return await modal.present();
//   // }

//   // goToPage(page: string){
//   //   this.router.navigate([page]);
//   // }

//   // selectVendor(index: any){
//   //   this.router.navigate(['/step3/' + index]);
//   // }
  
// }

