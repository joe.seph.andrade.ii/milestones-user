import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController, LoadingController, AlertController, ModalController, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { MilestonesService } from 'src/app/shared/services/milestones.service';
import { vendor_package, vendors, vendor_form, package_type } from 'src/app/shared/models/vendor';
import { Observable, Subscription } from 'rxjs';
import { ChatService } from 'src/app/shared/services/chat.service';
import { Chat } from 'src/app/shared/models/chat';
import { AngularFirestore } from '@angular/fire/firestore';
import { UsersService } from 'src/app/shared/services/users.service';
import { ms_event, transactions } from 'src/app/shared/models/milestones';
import { AddPaymentPage } from "./add-payment/add-payment.page";
import { myEnterAnimation } from "src/app/animation/enter";
import { myLeaveAnimation } from "src/app/animation/leave";

@Component({
  selector: 'app-msItemDetails',
  templateUrl: './msItemDetails.page.html',
  styleUrls: ['./msItemDetails.page.scss'],
})
export class MsItemDetailsPage implements OnInit, OnDestroy {
  public placeholder : string = '../../../assets/images/placeholder-square.jpg';
  public index: any;
  public item = {} as vendor_form;
  public vendors : Observable<vendors[]>;
  public event = {} as ms_event;

  public paymentArray = [];
  public new_flag = false;
  private subs : Array<Subscription> = [];

  public totals = {
    'paidAmount' : 0,
    'balance': 0,
    'totalAmount': 0,
  };

  constructor( private navCtr: NavController,
               private alertCtr: AlertController,
               private msServ: MilestonesService,
               private chtServ: ChatService,
               private loadCtr: LoadingController,
               private route: ActivatedRoute,
               private modCtrl: ModalController,
               private toastCtr: ToastController,
               private userServe: UsersService,) { 
                   
    this.getParams();
    
  }

  calcTotals(){
    let reg = this.event.packages[this.index].vendorPackage.regularPrice;
    let pct = this.event.packages[this.index].vendorPackage.discountRate;

    this.totals.paidAmount = this.totals.balance = this.totals.totalAmount = 0;
    this.totals.totalAmount = reg - (reg * pct);

    if (this.event.packages[this.index].transactions) {
      this.event.packages[this.index].transactions.forEach( data => {
        if (data.status == 'TVR') {
          this.totals.paidAmount += data.paidAmount;
        }
      });

      this.totals.balance = this.totals.totalAmount - this.totals.paidAmount;
    }else{
      this.event.packages[this.index].transactions = [];
    }

    this.paymentArray = this.event.packages[this.index].transactions;
   
  }

  async getParams(){
    await this.route.params.subscribe(idx=>{
      this.index = idx.idx   
      this.item = this.msServ.milestonesForm.controls['step2Form'].value[this.index];
      this.getVendors();
    });
  }

  async getVendors(){
    this.vendors = await this.msServ.getVendorPackage(this.item.vendor_type);
  }

  ngOnDestroy(){
    this.subs.forEach( sub => {
      sub.unsubscribe();
    });
  }

  ngOnInit() {
    let sub = this.msServ.getEventInfo().subscribe(event=>{
      this.event = event;
      this.calcTotals();
    });

    this.subs.push(sub);
  }

  goBackToPage(){
    this.navCtr.navigateBack('/msChecklist');
  }

  goToPage(page: string){
    this.navCtr.navigateForward([page]);
    // this.router.navigate([page]);
  }

  deletePackage( v_package: vendor_package, payment:string){
    
    if (payment == 'Pending') {
      this.deleteAlert(v_package);
    }else{
      this.pendingAlert();
    }
    
  }

  async addPayment(vendor: vendors, idx : number, mode: string, pay: transactions){

    const modal = await this.modCtrl.create({
      component: AddPaymentPage,
      enterAnimation: myEnterAnimation,
      leaveAnimation: myLeaveAnimation,
      componentProps: {
        mode: mode,
        transaction: pay,
        vendor_idx: this.index,
        packageId: vendor.packages[idx].vendorPackageId,
      }
    });

    modal.onDidDismiss().then(ret => {
      if (ret.role == 'success') {
        this.paymentArray.push(ret.data);
        this.calcTotals();
        this.new_flag = true;
        this.newReceipt(vendor, idx);
      }
    });

    return await modal.present();
  }

  uploadPayments(){
    this.loadCtr.create({
      message: 'Uploading your payment receipts.',
      spinner: 'dots',
    }).then(async (res) => {
        res.present().then( () => {
          this.msServ.updatePackage(this.index, this.paymentArray).then( (result) => {
            if (result) {
              this.presentToast('Payment transaction(s) submitted!','success');
              this.goBackToPage();
            }else{
              this.presentToast('Something went wrong!','error');
            }
            res.dismiss();
          });
        });
    });
  }

  async presentToast(msg: string, type: string) {
    const toast = await this.toastCtr.create({
      message: msg,
      duration: 2000,
      position: 'top',
      color: type,
    });
    toast.present();
  }

  async newReceipt(vendor: vendors, idx : number) {
    const alert = await this.alertCtr.create({
      header: 'More?',
      message: 'Do you want to add another receipt?',
      buttons: [
        {
          text: 'no',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {}
        }, {
          text: 'yes',
          handler: () => {
            this.addPayment(vendor, idx, 'add', null);
          }
        }
      ]
    });

    await alert.present();
  }

  async pendingAlert() {
    const alert = await this.alertCtr.create({
      header: 'Oopssie!',
      message: "This booking is already in progress, " +
               "and payments may have been made. Via app cancellation not allowed, " + 
               "please contact milestones. (032) 255-1234",
      buttons: ['OK']
    });

    await alert.present();
  }


  async deleteAlert( v_package: vendor_package ){

    const alert = await this.alertCtr.create({
      header: 'Delete package',
      message: 'Are you sure you want to delete this package?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {}
        }, {
          text: 'Delete',
          handler: () => {
            this.msServ.deletePackage(v_package).then(res => {
              if (res) {
                this.goToPage('/msChecklist');
              }else{
                console.log('toast here');
              }
            });
            
          }
        }
      ]
    });

    await alert.present();
  }

  async message(vendor: vendors, vendorPackage: vendor_package) {
    let _vendor = Object.assign({}, vendor); //create a new object reference
    delete _vendor.packages;

    let userId:string;
    await this.userServe.getUserFirebase().subscribe( data => {
      console.log('data', data)
      userId = data.userId;
      
      const chat: Chat = { 
        user : userId,
        vendor: _vendor,
        vendorPackageId: vendorPackage.vendorPackageId,//vendor.packages[0].vendorPackageId,
        eventId: this.event.eventId,
        createdAt: new Date().getTime().toString(),
      };
  
      this.loadCtr.create({
        message: 'Checking messages...',
        spinner: 'dots',
        duration: 10000,
      }).then(async (res) => {
          res.present();
          let isChatExist =  await this.chtServ.isChatExist(vendor, chat);
          
          if(isChatExist) { 
            let chatItem: Chat = await this.chtServ.getChatItem(chat)
            this.goToPage('/chat/' + chatItem.chatId + '/' + this.index + '/details');    
          } else {
            let chatItem: Chat = await this.chtServ.createChat(chat);
            this.goToPage('/chat/' + chatItem.chatId + '/' + this.index + '/details');   
            //create new chat
          }
          res.dismiss();
      });
    });


  }


  getClass(status: boolean){
    switch (status) {
      case true:
        return 'verified';
        break;

      case false:
        return 'waiting';
        break;
    
    }
  }

  toggleIcon(type: string){
    
    switch (type) {
      case 'DECORS':
        return 'ms-Icon ms-Icon--Balloons';
        break;

      case 'FOOD':
        return 'ms-Icon ms-Icon--Brunch';
        break;

      case 'VENUE':
        return 'ms-Icon ms-Icon--MapPin';
        break;

      case 'TABLES':
        return 'ms-Icon ms-Icon--StepShared';
        break;

      case 'LIGHTS AND SOUNDS':
        return 'ms-Icon ms-Icon--Lightbulb';
        break;

      case 'TRANSPORTATION':
        return 'ms-Icon ms-Icon--Car';
        break;

      case 'ACCOMMODATION':
        return 'ms-Icon ms-Icon--Hotel';
        break;

      case 'GROOMING':
        return 's-Icon ms-Icon--Cut';
        break;
  
      case 'TALENTS':
        return 's-Icon ms-Icon--Microphone';
        break;

      case 'CRAFTS':
        return 'ms-Icon ms-Icon--GreetingCard';
        break;

      case 'MULTIMEDIA':
        return 'ms-Icon ms-Icon--MSNVideos';
        break;

      default:
        return 'ms-Icon ms-Icon--SIPMove';
        break;
    }
  }


}
