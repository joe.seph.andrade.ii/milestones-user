import { Component, OnInit } from '@angular/core';
import { transactions } from "src/app/shared/models/milestones";
import { ModalController, NavParams, ToastController } from '@ionic/angular';
import { MilestonesService } from 'src/app/shared/services/milestones.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { vendors } from 'src/app/shared/models/vendor';

@Component({
  selector: 'app-add-payment',
  templateUrl: './add-payment.page.html',
  styleUrls: ['./add-payment.page.scss'],
})
export class AddPaymentPage implements OnInit {

  public currOpt = {
    align: "left",
    allowNegative: false,
    allowZero: false,
    decimal: ".",
    precision: 2,
    prefix: "₱ ",
    suffix: "",
    thousands: ",",
    nullable: false
  };

  test: any = 0;

  public payment = {} as transactions;
  private v_idx: number;
  public mode: string;

  constructor( private modCtr: ModalController,
               private navPar: NavParams,
               private toastCtr: ToastController,
               private msServ: MilestonesService) {
    this.v_idx = this.navPar.get('vendor_idx');
    this.mode  = this.navPar.get('mode');
    
    if (this.mode == 'view') {
      this.payment = this.navPar.get('transaction');
    }else{
      this.payment.paidAmount = 0;
    }

    this.payment.vendorPackageId = this.navPar.get('packageId');
  }

  ngOnInit() {
    
  }

  closeModal(){
    this.modCtr.dismiss(null,'close');
  }

  addPayment(){
    if (this.payment.paidAmount != 0 ) {
      this.payment.receiptUrl = 'https://omextemplates.content.office.net/support/templates/en-us/lt16410220.png';
      this.payment.status = 'TQD'; // TVR - Verified // TRJ - Rejected // TQD - Queued
      this.payment.paymentDate = new Date(this.payment.paymentDate).getTime();
      this.modCtr.dismiss(this.payment,'success');
    }else{
      this.closeModal();
    }
    
  }


}
