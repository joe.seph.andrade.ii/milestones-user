import { Component, OnInit } from '@angular/core';
import { ModalController, LoadingController, AlertController } from '@ionic/angular';
import { vendors } from 'src/app/shared/models/vendor';
import { NavParams } from '@ionic/angular';


@Component({
  selector: 'app-coordinatordetails',
  templateUrl: './coordinatordetails.page.html',
  styleUrls: ['./coordinatordetails.page.scss'],
})
export class CoordinatordetailsPage implements OnInit {

  public coord = {} as vendors;

  constructor(  private modalController: ModalController,
	              private loadCtr: LoadingController,
                private alertCtr: AlertController,
                private navParams: NavParams) { 
    this.coord = this.navParams.get('data');
  }

  ngOnInit() {
  }

  async select(){
    await this.modalController.dismiss(this.coord,'select');
  }

  toggleIcon(type: string){
    
    switch (type) {
      case 'DECORS':
        return 'ms-Icon ms-Icon--Balloons';
        break;

      case 'FOOD':
        return 'ms-Icon ms-Icon--Brunch';
        break;

      case 'VENUE':
        return 'ms-Icon ms-Icon--MapPin';
        break;

      case 'TABLES':
        return 'ms-Icon ms-Icon--StepShared';
        break;

      case 'LIGHTS_AND_SOUNDS':
        return 'ms-Icon ms-Icon--Lightbulb';
        break;

      case 'TRANSPORTATION':
        return 'ms-Icon ms-Icon--Car';
        break;

      case 'ACCOMMODATION':
        return 'ms-Icon ms-Icon--Hotel';
        break;

      case 'GROOMING':
        return 'ms-Icon ms-Icon--Cut';
        break;
  
      case 'TALENTS':
        return 'ms-Icon ms-Icon--Microphone';
        break;

      case 'CRAFTS':
        return 'ms-Icon ms-Icon--GreetingCard';
        break;

      case 'MULTIMEDIA':
        return 'ms-Icon ms-Icon--MSNVideos';
        break;

      default:
        return 'ms-Icon ms-Icon--SIPMove';
        break;
    }
  }

}
