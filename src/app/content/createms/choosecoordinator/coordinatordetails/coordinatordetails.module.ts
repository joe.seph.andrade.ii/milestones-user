import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CoordinatordetailsPage } from './coordinatordetails.page';

const routes: Routes = [
  {
    path: '',
    component: CoordinatordetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CoordinatordetailsPage]
})
export class CoordinatordetailsPageModule {}
