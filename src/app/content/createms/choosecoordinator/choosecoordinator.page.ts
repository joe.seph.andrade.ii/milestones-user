import { Component, OnInit, OnDestroy } from '@angular/core';
import { CoordinatordetailsPage } from './coordinatordetails/coordinatordetails.page'
import { ModalController, NavController, AlertController, LoadingController, ToastController } from '@ionic/angular';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { coordinator, vendors } from 'src/app/shared/models/vendor';
import { ms_event } from 'src/app/shared/models/milestones';
import { MilestonesService } from 'src/app/shared/services/milestones.service';
import { VendorsService } from 'src/app/shared/services/vendors.service';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';


@Component({
  selector: 'app-choosecoordinator',
  templateUrl: './choosecoordinator.page.html',
  styleUrls: ['./choosecoordinator.page.scss'],
})
export class ChoosecoordinatorPage implements OnInit, OnDestroy {

  public selected = {} as vendors;
  public event : Observable<ms_event>;

  public coordinators : Observable<vendors[]>;
  private coordDetail = {} as coordinator;
  public state : Subscription;

  constructor( private modalController: ModalController,
               private navCtr: NavController,
               private alertCtr: AlertController,
               private toastCtr: ToastController,
               private loadCtr: LoadingController,
               private msServ: MilestonesService,
               private vendServ: VendorsService,
               private router: Router) { 

    this.event = this.msServ.getEventInfo();
    this.vendServ.fetchVendorsByType('COORDINATOR');
    
  }

  ngOnDestroy(){
    this.state.unsubscribe();
  }

  async ngOnInit() {

    await this.loadCtr.create({
      message: 'Fetching your coordinators!',
    }).then( res => {
      res.present();
      res.onDidDismiss().then( data => {
        if (data.role != 'false') {
          this.presentAlert();
        }
      });
    });

    this.coordinators = this.vendServ.getVendors();
    this.state = this.coordinators.subscribe( data => {
      if (data.length > 0) {
        this.loadCtr.getTop().then( res => {
          if (res != undefined) {
            this.loadCtr.dismiss(data,'false');
          }else{
          }
        })
      }
    });
  }

  async presentAlert() {
    const alert = await this.alertCtr.create({
      header: 'Ooops!',
      message: "No available coordinators in your city. :(",
      buttons: ['OK']
    });

    await alert.present();
  }

  navBack(){
    this.navCtr.navigateBack(['/choosetype']);
  }

  async openCoordinatorDetails ( coord: any) {
  
    const modal = await this.modalController.create({
        component: CoordinatordetailsPage,
        componentProps: {
          "data": coord,
        }
      });
    
    modal.onDidDismiss().then( dataReturned => {
  
      if (dataReturned.role == 'select') {
        this.selected = dataReturned.data;
      }
    });

    return await modal.present();
  }

  async selectCoord() {
    let response: boolean = false;

    if (this.selected.coordinatorPreferences) {
      const alert = await this.alertCtr.create({
        header: 'Confirm',
        message: "Do you want to use your chosen coordinator's vendor preferences list? You  can still edit and change it as you wish",
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              this.router.navigate(['/msChecklist']);
            }
          }, {
            text: 'AGREE',
            handler: () => {
              let i = 0;
              let sub = this.event.subscribe( data => {
                if (i == 0) {
                  i += 1;
                  this.coordDetail.type = this.selected.name;
                  this.coordDetail.coordinatorId = this.selected.vendorId;
                  if (this.selected.coordinatorPreferences) {
                    this.coordDetail.preferences = this.selected.coordinatorPreferences;
                  }
                  data.selfManaged = false;
                  data.coordinatorId = this.selected.vendorId;
                  this.msServ.setCoordinator(this.coordDetail, true);
                  this.msServ.setEventInfo(data);
                  
                  this.loadCtr.create({
                    message: 'Uploading your event to cloud ...',
                    spinner: 'dots',
                    // duration: 30000,
                  }).then((res) => {
                    res.present();
                    this.msServ.createMs().then( status => {
                      if (status) {
                        response = true;
                        this.presentToast('Milestone event draft saved!','success');
                        this.router.navigateByUrl('/msChecklist');    
                      }else{
                        this.presentToast('Saving failed, please check connections.','medium');
                      }
                      this.loadCtr.dismiss();
                    })
                    res.onDidDismiss().then(() => {  
                      if (!response) {
                        this.presentToast('Saving failed, please check your connections.','warning');
                      }        
                    });
                  });
                }
              });
  
              sub.unsubscribe();
              
            }
          }
        ]
      });
      await alert.present();
    }else{
      this.router.navigate(['/msChecklist']);
    }
  }

  async delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  async presentToast(msg: string, type: string) {
    const toast = await this.toastCtr.create({
      message: msg,
      duration: 2000,
      position: 'top',
      color: type,
    });
    toast.present();
  }
}
