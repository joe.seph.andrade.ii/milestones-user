import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoosecoordinatorPage } from './choosecoordinator.page';

describe('ChoosecoordinatorPage', () => {
  let component: ChoosecoordinatorPage;
  let fixture: ComponentFixture<ChoosecoordinatorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoosecoordinatorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoosecoordinatorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
