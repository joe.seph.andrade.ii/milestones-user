import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PrefModalPage } from './pref-modal.page';

const routes: Routes = [
  {
    path: '',
    component: PrefModalPage
  }
];

@NgModule({
  declarations: [PrefModalPage],

  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],

  exports: [
    // PrefModalPage
  ]
  
})
export class PrefModalPageModule {}
