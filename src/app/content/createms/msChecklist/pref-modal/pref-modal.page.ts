import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { coordinator } from "../../../../shared/models/vendor";
import { MilestonesService } from "src/app/shared/services/milestones.service";

@Component({
  selector: 'app-pref-modal',
  templateUrl: './pref-modal.page.html',
  styleUrls: ['./pref-modal.page.scss'],
})
export class PrefModalPage implements OnInit {


  public coordinator = {} as coordinator;
  public vendorPrefs = [];

  constructor( private navParam : NavParams,
               private msServ: MilestonesService,
               private modCtr: ModalController) {  
    // this.coordinator = this.navParam.get('coordinator');
    this.coordinator = this.msServ.getCoordinator();
  }

  ngOnInit() {
    this.coordinator.preferences.forEach( data => {
      this.vendorPrefs.push({
        type: data.type,
        selected: data.vendors[0].vendorId,
        vendors: data.vendors,
      });
    });
  }

  savePref(){
    let array = [];

    this.vendorPrefs.forEach(data => {
      
      let found = data.vendors.find( vendor => {
        // return ( data.selected == elem.vendorId);
        return ( data.selected == vendor.vendorId);
      })

      if (found) {
        array.push(
          { 
            name: data.type,
            vendorName: found.name,
            vendorId: found.vendorId,
          }
        );
      }
    });
    this.modCtr.dismiss(array,'select');
  }

  dismiss(){
    this.modCtr.dismiss(null,'cancel');
  }

}
