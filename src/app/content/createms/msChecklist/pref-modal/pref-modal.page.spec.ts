import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrefModalPage } from './pref-modal.page';

describe('PrefModalPage', () => {
  let component: PrefModalPage;
  let fixture: ComponentFixture<PrefModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrefModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrefModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
