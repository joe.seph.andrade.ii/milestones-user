import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalController, LoadingController, AlertController } from '@ionic/angular';
import { VendorsService } from "../../../../shared/services/vendors.service";
import { ventype_status } from "../../../../shared/models/vendor";
import { NavParams } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-itemsmodal',
  templateUrl: './itemsmodal.page.html',
  styleUrls: ['./itemsmodal.page.scss'],
})
export class ItemsmodalPage implements OnInit, OnDestroy {

  public vendorTypes: Array<string> = [];
  public activeTypes = [];
  public vendorTypeForm: FormGroup;

  constructor( private modalController: ModalController,
               private VenSer: VendorsService, 
               private navParams: NavParams,
               private loadCtr: LoadingController,
               private alertCtr: AlertController,
               private formBuilder: FormBuilder
    ) { 
      
      this.activeTypes = this.navParams.get('items');
      this.vendorTypeForm = formBuilder.group({
        type: ['', Validators.required],
    });


    }

ngOnDestroy(){
  this.vendorTypes = [];
}


async ngOnInit() {
  let modtop = this.modalController.getTop();

  await this.loadCtr.create({
    message: 'Fetching categories...',
    // duration: 5000
  }).then( res => {
    res.present();
    res.onDidDismiss().then( data => {
      if (data.role != 'success') {
        this.presentAlert();
        modtop.then( act => {
          act.dismiss(undefined,'error');
        });
      }
    });
  });

    this.vendorTypeForm = this.formBuilder.group({});
    this.VenSer.fetchVendorEnum();

    let i = 0;
    this.VenSer.getVendorEnum().subscribe(body=>{
      
      if (body != undefined && i == 0) {
        i++;
        body.forEach(element => {
          let found = this.activeTypes.find(function(data: FormGroup) {
            let rev : any;
            if (data.controls['vendor_type'].value.type) {
               rev = data.controls['vendor_type'].value.type.split(' ').join('_');
            }else{
              rev = data.controls['vendor_type'].value.split(' ').join('_');
            }
            return element === rev;
          });
  
          if (!found) {
            let str = element.split('_').join(' ');
            this.vendorTypes.push(str);
            this.vendorTypeForm.addControl(str, this.formBuilder.control(false));
          }
        });

        let top = this.loadCtr.getTop();
        top.then( disp => {
          if (disp) {
            disp.dismiss(this.vendorTypes,'success');
          }
        });
      }
      
    });

  }

  async presentAlert() {
    const alert = await this.alertCtr.create({
      header: 'Ooops!',
      message: "Something went wrong, please check your internet connection.",
      buttons: ['OK']
    });

    await alert.present();
  }

  onSubmit(data:any){

  }

  async dismiss() {
    await this.modalController.dismiss(this.vendorTypeForm.value,'add');
  }

  async addOthers(){
    await this.modalController.dismiss(false,'others');
  }

}
