import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsmodalPage } from './itemsmodal.page';

describe('ItemsmodalPage', () => {
  let component: ItemsmodalPage;
  let fixture: ComponentFixture<ItemsmodalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsmodalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsmodalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
