import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ItemsmodalPage } from "./itemsmodal/itemsmodal.page";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { msChecklist } from './msChecklist.page';
import { PrefModalPage } from "./pref-modal/pref-modal.page";

const routes: Routes = [
  {
    path: '',
    component: msChecklist
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [msChecklist, ItemsmodalPage, PrefModalPage],
  entryComponents: [ ItemsmodalPage, PrefModalPage],
})

export class msChecklistPageModule {}
