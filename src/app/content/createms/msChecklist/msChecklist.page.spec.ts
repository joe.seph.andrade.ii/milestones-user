import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { msChecklist } from './msChecklist.page';

describe('msChecklist', () => {
  let component: msChecklist;
  let fixture: ComponentFixture<msChecklist>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ msChecklist ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(msChecklist);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
