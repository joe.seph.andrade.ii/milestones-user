import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ItemsmodalPage } from "./itemsmodal/itemsmodal.page"
import { PrefModalPage } from "./pref-modal/pref-modal.page";
import { MilestonesService } from "../../../shared/services/milestones.service";
import { VendorsService } from "../../../shared/services/vendors.service";
import { ms_event, event_packages } from "../../../shared/models/milestones";
import { ventype_status, coordinator } from "../../../shared/models/vendor";
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { FormBuilder, FormArray } from '@angular/forms';
import { myEnterAnimation } from "src/app/animation/enter";
import { myLeaveAnimation } from "src/app/animation/leave";
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-msChecklist',
  templateUrl: './msChecklist.page.html',
  styleUrls: ['./msChecklist.page.scss'],
})
export class msChecklist implements OnInit, OnDestroy {
  private dataReturned:any;
  public event = {} as ms_event;
  public vendors = new BehaviorSubject<string[]>(undefined);
  public arrayTypes: Array<ventype_status> = [];
  public  step2Form: FormArray ;
  public  formArrays = [];
  private coordinator = {} as coordinator;

  private subs : Array<Subscription> = [];
  private pref : string = 'false';
  
  constructor( private modalController: ModalController,
               private router: Router,
               private formBuilder: FormBuilder,
               private msServ: MilestonesService,
               private VenSer: VendorsService,
              ) { 
  }

  ngOnDestroy(): void { 
    this.subs.forEach( sub => {
      sub.unsubscribe();
    });
  }

  initDisplay(){
    
    let items = this.msServ.milestonesForm.controls['step2Form'].value;
    
    if (items.length != undefined) {
      this.step2Form = this.formBuilder.array(items);
    }
  }

  ngOnInit() {
    let sub = this.msServ.getEventInfo().subscribe(event=>{
      
      let sub0 = this.msServ.getPrefShow().subscribe(flag=>{
        if (flag) {
          this.coordinator = this.msServ.getCoordinator();
          this.preferenceModal(this.coordinator);
          this.msServ.setPrefShow(false);
        }
      });

      this.subs.push(sub0);
      this.event = event;
      if (this.event.packages.length) {
        this.initItems();
        this.msServ.fetchVendorPackages();
      }
    });

    this.subs.push(sub);
  }

  initItems(){
    if (this.event.packages) {
      if (!this.formArrays.length) {
        this.event.packages.forEach( vendors => {
          this.patchForm(vendors);
        });
  
        this.step2Form = this.formBuilder.array(this.formArrays);
        this.msServ.milestonesForm.setControl('step2Form', this.step2Form);
        let item = this.msServ.milestonesForm.controls['step2Form'].value;
        if (item.length != undefined) {
          this.step2Form.value.forEach( data => {
            let found = item.find( elem => {
              if (elem.vendor_id == data.vendor_id) {
                elem = data;
              }
              return (elem.vendor_id == data.vendor_id)
            });
          });
          this.step2Form = this.formBuilder.array(item);

          this.formArrays = [];
          item.forEach( data => {
            let _form = this.msServ.createStep2Form();
            _form.patchValue({
              vendor_type: data.vendor_type,
              vendor_id: data.vendor_id,
              vendor_name: data.vendor_name,
              vendor_status: true,
              vendor_payment: 'Pending',
              vendor_package_id: data.vendor_package_id,
            });
            this.formArrays.push(_form);
          });

        }
        // else{
        //   this.step2Form = this.formBuilder.array(this.formArrays);
        // }
        // this.msServ.milestonesForm.setControl('step2Form', this.step2Form);
      }
    }
  }

  patchForm(vendors: event_packages ){
    let _form = this.msServ.createStep2Form();
    _form.patchValue({
      vendor_type: vendors.vendorPackage.packageType.type,
      vendor_id: vendors.vendorId,
      vendor_name: vendors.vendorName,
      vendor_status: true,
      vendor_payment: 'Pending',
      vendor_package_id: vendors.vendorPackage.vendorPackageId
    });

    this.formArrays.push(_form);
  }

  async preferenceModal(coordinator: any){

    const modal_pref = await this.modalController.create({
      component: PrefModalPage,
      enterAnimation: myEnterAnimation,
      leaveAnimation: myLeaveAnimation,
      componentProps: {
        "coordinator": coordinator,
      }
    });

    modal_pref.onDidDismiss().then((retData) => {
      
      if (retData.data && retData.role == 'select') {
        retData.data.forEach(data=>{
          let _form = this.msServ.createStep2Form();
          if (data) {
            let type = data.name.split('_').join(' ');
            _form.patchValue({
              vendor_type: type,
              vendor_id: data.vendorId,
              vendor_name: data.vendorName,
              vendor_status: false,
            });
            
            this.formArrays.push(_form);
          }
        });
        this.step2Form = this.formBuilder.array(this.formArrays);
        this.msServ.milestonesForm.setControl('step2Form', this.step2Form);
        }
      }); 

    return await modal_pref.present();
  }
  

  toggleClass(type: string){

    switch (type) {
      case 'DECORS':
        return 'ms-card-item-deco';
        break;

      case 'FOOD':
        return 'ms-card-item-deco';
        break;

      case 'VENUE':
        return 'ms-card-item-venue';
        break;

      case 'TABLES':
        return 'ms-card-item-tables';
        break;

      case 'LIGHTS AND SOUNDS':
        return 'ms-card-item-lightsandsounds';
        break;

      case 'TRANSPORTATION':
        return 'ms-card-item-transportation';
        break;

      case 'ACCOMMODATION':
        return 'ms-card-item-accommodation';
        break;

      case 'GROOMING':
        return 'ms-card-item-grooming';
        break;
  
      case 'TALENTS':
        return 'ms-card-item-talents';
        break;

      case 'CRAFTS':
        return 'ms-card-item-crafts';
        break;

      case 'MULTIMEDIA':
        return 'ms-card-item-multimedia';
        break;

      default:
        return 'ms-card-item-others';
        break;
    }
  }

  toggleIcon(type: string){
    
    switch (type) {
      case 'DECORS':
        return 'ms-Icon ms-Icon--Balloons';
        break;

      case 'FOOD':
        return 'ms-Icon ms-Icon--Brunch';
        break;

      case 'VENUE':
        return 'ms-Icon ms-Icon--MapPin';
        break;

      case 'TABLES':
        return 'ms-Icon ms-Icon--StepShared';
        break;

      case 'LIGHTS AND SOUNDS':
        return 'ms-Icon ms-Icon--Lightbulb';
        break;

      case 'TRANSPORTATION':
        return 'ms-Icon ms-Icon--Car';
        break;

      case 'ACCOMMODATION':
        return 'ms-Icon ms-Icon--Hotel';
        break;

      case 'GROOMING':
        return 'ms-Icon ms-Icon--Cut';
        break;
  
      case 'TALENTS':
        return 'ms-Icon ms-Icon--Microphone';
        break;

      case 'CRAFTS':
        return 'ms-Icon ms-Icon--GreetingCard';
        break;

      case 'MULTIMEDIA':
        return 'ms-Icon ms-Icon--MSNVideos';
        break;

      default:
        return 'ms-Icon ms-Icon--SIPMove';
        break;
    }
  }

  toggleStatus(status: boolean){
    if (!status) {
      return 'fas fa-plus';
    }else{
      return 'fas fa-check';
    }
  }

  async openModal() {

    // this.initItems();

    const modal = await this.modalController.create({
      component: ItemsmodalPage,
      enterAnimation: myEnterAnimation,
      leaveAnimation: myLeaveAnimation,
      componentProps: {
       "items": this.formArrays,
      }
    });
 
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data) {

        let _formOthers = this.msServ.createStep2Form();
        if (!dataReturned.data && dataReturned.role == 'others') {
          _formOthers.patchValue({
            vendor_type: 'OTHERS',
            vendor_id: null,
            vendor_name: '',
            vendor_status: false,
          });
          this.formArrays.push(_formOthers);
          this.step2Form = this.formBuilder.array(this.formArrays);
          
        }else if (dataReturned.role == 'add'){
          this.dataReturned = dataReturned.data;
          Object.keys(this.dataReturned).forEach(data=>{
            let _form = this.msServ.createStep2Form();
            if (this.dataReturned[data]) {

              _form.patchValue({
                vendor_type: data,
                vendor_id: null,
                vendor_name: '',
                vendor_status: false,
              });
              
              this.formArrays.push(_form);
            }
          });
          this.step2Form = this.formBuilder.array(this.formArrays);
        }

        this.msServ.milestonesForm.setControl('step2Form', this.step2Form);
      }
      
    });
    
    return await modal.present();
  }

  goToPage(page: string){
    this.router.navigate([page]);
  }

  

  selectVendor(type: any, idx: any){
    if (type.vendor_id && type.vendor_package_id) {
      this.router.navigateByUrl("msItemDetails/" + idx);
    }else{
      this.router.navigateByUrl("ms-packages/" + idx);
    }
  }

  
  deleteMs(){

  }
}

