import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController, NavController, LoadingController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl} from '@angular/forms';
import { ms_event } from "../../../shared/models/milestones";
import { MilestonesService } from "../../../shared/services/milestones.service";

@Component({
  selector: 'app-step1',
  templateUrl: './msForm.page.html',
  styleUrls: ['./msForm.page.scss'],
})
export class Step1Page implements OnInit {

  public registerForm: AbstractControl;
  private eventObj = {} as ms_event;
  private loading: any;

  public currOpt = {
    align: "left",
    allowNegative: false,
    allowZero: false,
    decimal: ".",
    precision: 2,
    prefix: "₱ ",
    suffix: "",
    thousands: ",",
    nullable: false
  };

  public numOpt = {
    align: "left",
    allowNegative: false,
    allowZero: false,
    decimal: "",
    precision: 0,
    prefix: "",
    suffix: "",
    thousands: ",",
    nullable: false
  };
  constructor(  private router: Router,
                private msServ: MilestonesService,
                private loadCtr: LoadingController,
                private navCtr: NavController,
                public  formBuilder: FormBuilder, 
                private toastCtrL: ToastController,
              ) { 
    this.registerForm = this.msServ.milestonesForm.controls['step1Form'];
  }

  ngOnInit() {
    this.registerForm.value.milestone_title = '';
    this.registerForm.value.milestone_type = '';
    this.registerForm.value.milestone_budget = 0;
    this.registerForm.value.milestone_guestspax = 0;
    this.registerForm.value.milestone_date = '';
    this.registerForm.value.milestone_city = '';
  }


  goToPage(page: string){
    this.router.navigate([page]);
  }

  navigateBack(page: string){
    this.navCtr.navigateBack(page);
  }

  async onSubmit(){
    let data = this.registerForm.value;
    if (this.registerForm.valid) { 
      

      this.eventObj.name = data.milestone_title;
      this.eventObj.eventType = data.milestone_type;
      this.eventObj.budget = data.milestone_budget;
      this.eventObj.targetDateTime = new Date(data.milestone_date).getTime().toString();
      this.msServ.milestonesForm.controls['step1Form'].patchValue(this.registerForm.value);
      this.msServ.setEventInfo(this.eventObj);
      this.goToPage('/choosetype');    

      // this.loadCtr.create({
      //   message: 'Uploading your event to cloud ...',
      //   spinner: 'dots',
      //   // duration: 30000,
      // }).then((res) => {
      //   res.present();
        
      //   this.eventObj.name = data.milestone_title;
      //   this.eventObj.eventType = data.milestone_type;
      //   this.eventObj.budget = data.milestone_budget;
      //   this.eventObj.targetDateTime = new Date(data.milestone_date).getTime().toString();
      //   this.msServ.milestonesForm.controls['step1Form'].patchValue(this.registerForm.value);
      //   this.msServ.setEventInfo(this.eventObj);

        
      //   this.msServ.createMs().then( status => {
      //     if (status) {
      //       response = true;
      //       this.presentToast('Milestone event draft saved!','success');
      //       this.goToPage('/choosetype');    
      //     }else{
      //       this.presentToast('Saving failed, please check connections.','medium');
      //     }
      //     this.loadCtr.dismiss();
      //   })
      //   res.onDidDismiss().then(() => {  
      //     if (!response) {
      //       this.presentToast('Saving failed, please check your connections.','warning');
      //     }        
      //   });
      // });

      
    }else{
      this.presentToast('Please make sure to complete all details.','secondary');
    }
  }

  async presentToast(msg: string, type: string) {
    const toast = await this.toastCtrL.create({
      message: msg,
      duration: 2000,
      position: 'top',
      color: type,
    });
    toast.present();
  }

  validation_messages = {
    'milestone_title': [
      { type: 'required', message: 'Milestone title is required.' }
    ],
    'milestone_type': [
      { type: 'required', message: 'Milestone type is required.' }
    ],
    'milestone_date': [
      { type: 'required', message: 'Milestone date & time is required'}
    ],
    'milestone_budget': [
      { type: 'required', message: 'Milestone budget is required'}
    ],
    'milestone_guestspax': [
      { type: 'required', message: 'Milestone tentative number of guests is required'}
    ],
  };
}
