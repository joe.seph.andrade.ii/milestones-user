import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, AlertController, Platform } from '@ionic/angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { environment } from '../../..//environments/environment';
import { UsersService } from "../../shared/services/users.service";
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as firebase from 'firebase';
import { userDocument } from "../../shared/models/users";
import { UtilsService } from 'src/app/shared/services/utils.service';

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})
export class LoginPage {

  private userDoc = {} as userDocument; // This is used for user storage
  public loginForm: FormGroup;

  constructor(private userService: UsersService,
              private formBuilder: FormBuilder,
              private router: Router,
              private fb: Facebook,
              private fireAuth: AngularFireAuth,
              private google: GooglePlus,
              private platform: Platform,
              private utilServ: UtilsService,
              public  loadingCtr: LoadingController,
              public  alertController: AlertController){
    this.loginForm = this.formBuilder.group({
      email:     [this.userDoc.email,     Validators.compose([Validators.maxLength(60), Validators.pattern("[a-zA-Z0-9_\\.\\+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-\\.]+"), Validators.required])],
      password:  [this.userDoc.password,  Validators.compose([Validators.maxLength(60), Validators.required])],
    });
  }

  async fb_login() {

    this.loadingCtr.create({
      message: 'Checking account...',
      spinner: 'dots',
      duration: 10000,
    }).then((resp) => {

      resp.present().then(() => {

        this.fb.getLoginStatus().then((res)=>{
          if (res.status == 'connected') {
            this.onLoginSuccess(res.authResponse.accessToken).then( bol => {
              resp.dismiss();
              if (bol) {
                this.router.navigateByUrl('/tabs');
              }else{
                this.signUpFaceboook();
              }
            });
          }else{
            this.signUpFaceboook();
          }
        }).catch(er=>console.log(er));
      });
    });


    // Hard Coded for Login ( JV Purposes )
    // this.userDoc.authToken = "EAAE1q8LGbBoBAHRdJV1yLWpUZAEYNf8Q7tkrk32jmpqcF1w1JJqQmYlLFH4CwdZCGvbJZALmZCE6ci56KC3ZAMuufWCKJTDlJe2x4s6gmahEtgiGe8543rLLlZBNNBD3oIqZAUD3PNYsW13K1QiR8UzZA5t4xxpPFnswuwYZBObC4ASTd7BW7y1Qg6XeZBG2m7FDqDVgltTfNNPAZDZD";
    // this.userDoc.email = "liford99@yahoo.com";
    // this.userDoc.firstName =  "Clifford";
    // this.userDoc.lastName = "Diaz";
    // this.userDoc.loginType = "facebook";
    // this.userDoc.photoUrl = "https://graph.facebook.com/10218956857967984/picture?width=1024&height=1024";
    // this.userDoc.userId = "2C5jXatx12T2eS41jRQ4Amj0KSB2";

    // this.onLoginSuccess(this.userDoc.authToken).then( bol => {
    //   if (bol){
    //     this.router.navigateByUrl('/tabs');
    //   }
    // });
  }p

  email_login(creds){
    this.userService.loginEmail(creds.email, creds.password).then(()=>{
      this.router.navigateByUrl('/tabs');
    }).catch((er)=>{
      this.utilServ.showToast(er);
    });
  }

  signUp(){
    const userDoc = {
      loginType: 'email'
    } as userDocument;

    this.userService.setUserFirebase(userDoc);
    this.router.navigateByUrl('/verifypage1');
  }

  signUpFaceboook(){
    const permissions = ["public_profile", "email"];
    this.fb.login(permissions).then((response: FacebookLoginResponse) => {

      this.onLoginSuccess(response.authResponse.accessToken).then( bol => {
        if (bol) {
          this.router.navigateByUrl('/tabs');
        }else{
          this.fb.api("/me?fields=name,email", permissions).then(result => {
            this.userDoc.photoUrl = "https://graph.facebook.com/" + response.authResponse.userID + "/picture?width=1024&height=1024";
            let str = result.name.split(" ");
            
            this.userDoc.firstName   = str[0];
            this.userDoc.lastName    = str[1];
            this.userDoc.email       = result.email;
            this.userDoc.authToken   = response.authResponse.accessToken;
            this.userDoc.loginType   = 'facebook';
          }).catch(e => {
            console.log(e);
          });

          this.userService.setUserFirebase(this.userDoc)
          this.router.navigateByUrl('/verifypage1');
        }
      });
    }).catch((error) => {
      console.log(error);
    });    
  }

  async onLoginSuccess(accessToken: string) {
    this.userService.token = accessToken; // This is CRITICAL for HTTP REQUESTS

    return new Promise<boolean>((resolve) => {
      const credential = firebase.auth.FacebookAuthProvider.credential(accessToken);

      this.fireAuth.auth.signInWithCredential(credential).then((response) => {
        if (response) {
             // Remove this two lines after clyde fixes the user doc mapping
            // this.userDoc.userId = response.user.uid;
            // this.userService.saveUserStorage(this.userDoc);
            // End here
            this.userService.fetchUserFirebase(response.user.uid).then( bol => {
              if (bol) {
                resolve(true);
              }else{
                resolve(false);
              }
            });
          }
        }).catch(() => {
          resolve(false)
        });
    });
  }

  onLoginError(err) {
    console.log(err);
  }

  async google_login() {
    let params;
    if (this.platform.is('android')) {
      params = {
        'webClientId': environment.googleWebClientId,
        'offline': true
      }
    }
    else {
      params = {}
    }
    this.google.login(params)
      .then((response) => {
        console.log(response);
        const { idToken, accessToken } = response
        this.onGoogleLoginSuccess(idToken, accessToken);
      }).catch((error) => {
        console.log(error)
        alert('error:' + JSON.stringify(error))
      });
  }

  onGoogleLoginSuccess(accessToken, accessSecret) {
    const credential = accessSecret ? firebase.auth.GoogleAuthProvider
        .credential(accessToken, accessSecret) : firebase.auth.GoogleAuthProvider
            .credential(accessToken);
    this.fireAuth.auth.signInWithCredential(credential)
      .then((response) => {
        this.router.navigate(["/tabs"]);
      }).catch(err => {
        console.log(err);
      })

  }

  viewGuest(){
    this.router.navigateByUrl('/tabs');
  }
}
