import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { CitiesPage } from "./cities/cities.page";
import { IonicModule } from '@ionic/angular';

import { Verifypage1Page } from './verifypage1.page';

const routes: Routes = [
  {
    path: '',
    component: Verifypage1Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Verifypage1Page, CitiesPage],
  entryComponents: [CitiesPage]
})
export class Verifypage1PageModule {}
