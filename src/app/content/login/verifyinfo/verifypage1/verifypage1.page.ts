import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { UsersService } from  "../../../../shared/services/users.service";
import { ModalController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { userDocument } from  "../../../../shared/models/users";
import { UtilsService } from 'src/app/shared/services/utils.service';
import { CitiesPage } from "./cities/cities.page";

@Component({
  selector: 'app-verifypage1',
  templateUrl: './verifypage1.page.html',
  styleUrls: ['./verifypage1.page.scss'],
})
export class Verifypage1Page implements OnInit {

  public registerForm: FormGroup;
  public datetime: Date;
  public userDoc = {} as userDocument;
  private count: number = 0;

  constructor(public formBuilder: FormBuilder,
              private userService: UsersService,
              private utilServ: UtilsService,
              private modCtr: ModalController,
              private toastCtrL: ToastController,
              private router: Router) {
                
    
    this.userService.getUserFirebase().subscribe( dat => {
      if (dat) {
        this.userDoc = dat;

        this.datetime = new Date();
        this.datetime.setFullYear(this.datetime.getFullYear() - 18);
        
        if (this.userDoc.loginType == 'email') {
          this.registerForm = formBuilder.group({
            firstname: [this.userDoc.firstName,       Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
            lastname:  [this.userDoc.lastName,        Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
            mobile:    [this.userDoc.contactNumber1,  Validators.compose([Validators.maxLength(11), Validators.pattern('^(09)\\d{9}$'), Validators.required])],
            email:     [this.userDoc.email,           Validators.compose([Validators.maxLength(60), Validators.pattern("[a-zA-Z0-9_\\.\\+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-\\.]+"), Validators.required])],
            con_email: ['',                           Validators.compose([Validators.maxLength(60), Validators.required])],
            password:  [this.userDoc.password,        Validators.compose([Validators.maxLength(30), Validators.pattern("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$"), Validators.required])],
            address:   [this.userDoc.address,         Validators.required],
            city:      [this.userDoc.city,            Validators.required],
            sex:       [this.userDoc.gender,          Validators.required],
            birthdate: [this.userDoc.birthday,        Validators.required],
          },{ validator: this.matchingFields('email', 'con_email') });
        }else{
          this.registerForm = formBuilder.group({
            firstname: [this.userDoc.firstName,       Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
            lastname:  [this.userDoc.lastName,        Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
            mobile:    [this.userDoc.contactNumber1,  Validators.compose([Validators.maxLength(11), Validators.pattern('^(09)\\d{9}$'), Validators.required])],
            address:   [this.userDoc.address,         Validators.required],
            city:      [this.userDoc.city,            Validators.required],
            sex:       [this.userDoc.gender,          Validators.required],
            birthdate: [this.userDoc.birthday,        Validators.required],
          });
        }
        
      }
    }).unsubscribe();
  }

  checkLoginType(type){
    return (this.userDoc.loginType == type ? true: false);
  }


  matchingFields(field1, field2) {
    return form => {
      if (form.controls[field1].value != form.controls[field2].value) {
        return { mismatchedFields: true };
      }
    };
  }

  ngOnInit() {
  
  }

  ionViewDidEnter(){
    this.count = 0;
  }

  async openCity() {
    const modal = await this.modCtr.create({
    component: CitiesPage,
    componentProps: {}
    });
    await modal.present();

    await modal.onDidDismiss().then( ret => {
      if (ret.role == 'SUCCESS') {
        this.registerForm.controls['city'].patchValue(ret.data);
      }
    });
  }

  async presentToast() {
    const toast = await this.toastCtrL.create({
      message: 'Please make sure to complete all details. :)',
      duration: 2000,
      position: 'top',
      color: 'warning',
    });
    toast.present();
  }


  onSubmit(data: any){
    
    if (this.count == 0) {
      this.count += 1;

      this.userDoc.firstName      = data.firstname;
      this.userDoc.lastName       = data.lastname;
      this.userDoc.address        = data.address;
      this.userDoc.city           = data.city;
      this.userDoc.gender         = data.sex;
      this.userDoc.birthday       = data.birthdate;
      this.userDoc.contactNumber1 = data.mobile;

      if (this.userDoc.loginType == 'facebook' || this.userDoc.loginType == 'google') {
        this.userService.setUserFirebase(this.userDoc);
        this.router.navigate(["/verifypage2"]);
      }else{ // Email

        this.userService.createUser(data.email, data.password).then((res:firebase.User)=>{
          res.getIdToken().then( token =>{
            this.userDoc.authToken =  token;
            this.userDoc.userId = res.uid
            this.userService.setUserFirebase(this.userDoc);
            this.router.navigate(["/verifypage2"]);
          });
          
        }).catch((err)=>{
          this.utilServ.showToast(err);
        });
      }
    }
    

  }

  validation_messages = {
    // 'username': [
    //   { type: 'required', message: 'Username is required.' },
    //   { type: 'minlength', message: 'Username must be at least 5 characters long.' },
    //   { type: 'maxlength', message: 'Username cannot be more than 25 characters long.' },
    //   { type: 'pattern', message: 'Your username must contain only numbers and letters.' },
    //   { type: 'validUsername', message: 'Your username has already been taken.' }
    // ],
    'firstname': [
      { type: 'required', message: '* Name is required.' }
    ],
    'lastname': [
      { type: 'required', message: '* Last name is required.' }
    ],
    'address': [
      { type: 'required', message: '* Address is required'}
    ],
    'city': [
      { type: 'required', message: '* City is required'}
    ],
    'mobile': [
      { type: 'required', message: '* Mobile number is required'},
      { type: 'pattern',  message: '* Invalid number'},
    ],
    'sex': [
      { type: 'required', message: '* Gender is required'}
    ],
    'birthdate':[
      { type: 'required', message: '* Birthdate is required'}
    ],
    'email': [
      { type: 'required', message: '* Email is required.' },
      { type: 'pattern',  message: '* Enter a valid email.' }
    ],
    // 'phone': [
    //   { type: 'required', message: 'Phone is required.' },
    //   { type: 'validCountryPhone', message: 'Phone incorrect for the country selected' }
    // ],
    'con_email': [
      { type: 'required', message: '* Email did not match'},
    ],
    'password': [
      { type: 'required', message:  '* Password is required.' },
      { type: 'minlength', message: '* Password must be at least 5 characters long.' },
      { type: 'pattern', message:   '* Atleast 8 chars, 1 number and 1 special character' }
    ],
    // 'confirm_password': [
    //   { type: 'required', message: 'Confirm password is required' }
    // ],
    // 'matching_passwords': [
    //   { type: 'areEqual', message: 'Password mismatch' }
    // ],
    // 'terms': [
    //   { type: 'pattern', message: 'You must accept terms and conditions.' }
    // ],
  };

}
