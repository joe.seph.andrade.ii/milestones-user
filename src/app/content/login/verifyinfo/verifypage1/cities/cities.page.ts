import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { m_city } from 'src/app/shared/models/util';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.page.html',
  styleUrls: ['./cities.page.scss'],
})
export class CitiesPage implements OnInit {

  public  cityArray : Array<m_city> = [];
  private dataSource: Array<m_city> = [];

  public citySearch:string;

  constructor( private modCtr: ModalController) { 
    this.read_data();
  }

  read_data(){
    fetch("./assets/libs/cities/ph-cities.json").then(res => res.json())
    .then(json => {
      this.cityArray = this.dataSource = json;
    });
  }

  ngOnInit() {

  }

  searchCity($ev){
    let input:string = $ev.target.value;
    this.cityArray = this.dataSource;

    if (input.trim() == '') {
      return;
    }
    
    this.cityArray = this.dataSource.filter((data)=>{
      if (data.city.toLowerCase().includes(input.toLowerCase())) {
        return true;
      }
      
      return false;
    });
  }

  selectCity($ev, city){
    this.modCtr.dismiss(city.city,"SUCCESS");
  }

}
