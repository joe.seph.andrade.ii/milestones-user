import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Verifypage1Page } from './verifypage1.page';

describe('Verifypage1Page', () => {
  let component: Verifypage1Page;
  let fixture: ComponentFixture<Verifypage1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Verifypage1Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Verifypage1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
