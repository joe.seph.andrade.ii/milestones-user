import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Verifypage2Page } from './verifypage2.page';

describe('Verifypage2Page', () => {
  let component: Verifypage2Page;
  let fixture: ComponentFixture<Verifypage2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Verifypage2Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Verifypage2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
