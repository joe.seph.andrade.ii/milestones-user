import { Component, OnInit } from '@angular/core';
import { Camera } from '@ionic-native/camera/ngx';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { UsersService } from "../../../../shared/services/users.service";
import { userDocument } from "../../../../shared/models/users";
import { Router } from '@angular/router';
import { UtilsService } from 'src/app/shared/services/utils.service';

@Component({
  selector: 'app-verifypage2',
  templateUrl: './verifypage2.page.html',
  styleUrls: ['./verifypage2.page.scss'],
})
export class Verifypage2Page implements OnInit {

  // public image: SafeResourceUrl;
  public image: string;
  private options: any;
  public userDoc = {} as userDocument;

  constructor(private router: Router,
              private camera: Camera,
              private utilServ: UtilsService,
              private userService: UsersService,
              private sanitizer: DomSanitizer) { 

    this.userService.getUserFirebase().subscribe( dat => {
      this.userDoc = dat;
    })

    this.image = null;

    this.options = {
      quality: 20,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA
    }

  }


  ngOnInit() {
  }


  openCamera()
  { 
    // this.camera.getPicture(this.options).then((imageData) => {
    //   this.image = 'data:image/jpeg;base64,' + imageData;
    //   this.userDoc.selfieUrl = this.image;
    //  }, (err) => {
    //   this.utilServ.showToast('Unable to load camera');
    //  });

    this.image = this.userDoc.photoUrl = this.utilServ.getBase64();
  }

  clearImg(opt: string){
    this.image = null;
  }

  onSubmit(){
    this.userService.setUserFirebase(this.userDoc);
    this.router.navigate(["/verifypage3"])
  }

}
