import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { Verifypage2Page } from './verifypage2.page';

const routes: Routes = [
  {
    path: '',
    component: Verifypage2Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Verifypage2Page]
})
export class Verifypage2PageModule {}
