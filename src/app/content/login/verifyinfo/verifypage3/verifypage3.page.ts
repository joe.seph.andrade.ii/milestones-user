import { Component, OnDestroy, OnInit } from '@angular/core';
import { Camera } from '@ionic-native/camera/ngx';
import { UsersService } from "../../../../shared/services/users.service";
import { userDocument } from "../../../../shared/models/users";
import { Router } from '@angular/router';

import { ToastController, LoadingController } from '@ionic/angular';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { EMPTY, Observable, Subject } from 'rxjs';
import { catchError, finalize, takeUntil, tap } from 'rxjs/operators';
import { UtilsService } from 'src/app/shared/services/utils.service';
import { FbstorageService } from 'src/app/shared/services/fbstorage.service';

export interface MyData {
  name: string;
  filepath: string;
  size: number;
}

@Component({
  selector: 'app-verifypage3',
  templateUrl: './verifypage3.page.html',
  styleUrls: ['./verifypage3.page.scss'],
})
export class Verifypage3Page implements OnInit, OnDestroy {

  public front: string;
  public back: string;
  private options: any;
  public userDoc = {} as userDocument;

  private task: AngularFireUploadTask;
  private snapshot: Observable<any>;
  private UploadedFileURL: Observable<string>;
  private fileSize:number;
  public  progress: Observable<number>;
  public current: string;

  destroy$: Subject<null> = new Subject();
  constructor(private storage: AngularFireStorage, 
              private loadingCtr: LoadingController,
              private router: Router,
              private utilServ: UtilsService,
              private storServ: FbstorageService,
              private camera: Camera,
              private toastCtrl: ToastController,
              private userService: UsersService) { 

    this.userService.getUserFirebase().subscribe(dat => {
      this.userDoc = dat;
    });

    this.front = null;
    this.back  = null;

    this.options = {
      quality: 10,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA
    }}

    ngOnInit() {
    }

    ngOnDestroy() {
      this.destroy$.next(null);
    }

    async uploadFile( data_url : string, type: string) {
    
      const { downloadUrl$, uploadProgress$ } = this.storServ.uploadFile(`users/${this.userDoc.userId}/${type}.jpg`, data_url);

      this.progress = uploadProgress$;
      this.current = type;
      
      downloadUrl$
        .pipe(
          takeUntil(this.destroy$),
          catchError((error) => {
            this.utilServ.showToast(error);
            return EMPTY;
          }),
        )
        .subscribe((url) => {
          switch (type) {
            case 'selfie':
              this.userDoc.photoUrl = url;
              break;
            case 'ID Card':
              this.userDoc.governmentIdUrl = url;
              break;
          }
        });
    }
   
  
    openCamera(opt: string)
    {       
      
      if (opt == 'front') {
        this.front = this.userDoc.governmentIdUrl = this.utilServ.getBase64();
      }else{
        this.back = this.userDoc.governmentIdBackUrl = this.utilServ.getBase64();
      }

      // this.camera.getPicture(this.options).then((imageData) => {

      //   if (opt == 'front') {
      //     this.front = 'data:image/jpeg;base64,' + imageData; 
      //     this.userDoc.governmentIdUrl = this.front;
      //   }else{
      //     this.back = 'data:image/jpeg;base64,' + imageData; 
      //     this.userDoc.governmentIdBackUrl = this.back;
      //   }
      //  }, (err) => {
      //   this.presentToast('Unable to load camera', 'danger');
      //  });
    }
  
    clearImg(opt: string){
      if (opt == 'front') {
        this.front = null;
      }else{
        this.back = null;
      }
    }
  
    onSubmit(){
    
      // if (this.front != null && this.back != null) {
      if (this.front != null) {
        
        this.loadingCtr.create({
          message: 'Uploading, please wait...',
          spinner: 'bubbles',
          duration: 3000,
          translucent: true,
        }).then((res) => {

          res.present().then(() => {
            this.uploadFile(this.userDoc.photoUrl, 'selfie');
            this.uploadFile(this.userDoc.governmentIdUrl, 'ID Card');
          });
          
          res.onDidDismiss().then(() => {
            if (this.userDoc.photoUrl && this.userDoc.governmentIdUrl) {
              this.userService.setUserFirebase(this.userDoc);
              this.userService.createUserFirebase().then(() => {
                this.router.navigate(["/tabs"]);
              }).catch(err=>{
                this.utilServ.showToast('err');
              });
            }else{
              this.utilServ.showToast('Error uploading images, try completing your registration later.');
            }
          });
        });

      }else{
        this.presentToast('Please complete front/back images.','warning')
      }


      
    }

    presentToast(msg: string, color: string){
      this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: 'top',
        color: color,
      })
    }
  
}
