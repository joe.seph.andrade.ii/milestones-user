import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { Verifypage3Page } from './verifypage3.page';

const routes: Routes = [
  {
    path: '',
    component: Verifypage3Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AngularFireStorageModule,
    AngularFirestoreModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Verifypage3Page]
})
export class Verifypage3PageModule {}
