import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Verifypage3Page } from './verifypage3.page';

describe('Verifypage3Page', () => {
  let component: Verifypage3Page;
  let fixture: ComponentFixture<Verifypage3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Verifypage3Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Verifypage3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
