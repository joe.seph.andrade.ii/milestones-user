import { Component, OnInit } from '@angular/core';
import { userDocument } from "../../shared/models/users";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  public user = {} as userDocument;

  constructor() { }

  ngOnInit() {
  }

}
